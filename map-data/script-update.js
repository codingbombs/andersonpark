function serializeData(tmp, data) {
    let i = 0;
    tmp.spots.forEach(spot => {
        //apply thumbnail
        spot.tooltip_content.squares_settings.containers[0].settings.elements[0].options.image.url = `assets/imgs/apartment/${data[i].thumbnail}`
        spot.tooltip_content.squares_settings.containers[0].settings.elements[1].options.heading.text = `Căn hộ ${data[i].code}`
        spot.tooltip_content.squares_settings.containers[0].settings.elements[2].options.heading.text = `${data[i].total_area}m² | ${data[i].bedroom}PN - ${data[i].wc}WC`
        spot.tooltip_content.squares_settings.containers[0].settings.elements[3].options.button.link_to = `can-ho-${data[i].link_to}`
        spot.tooltip_content.squares_settings.containers[0].settings.elements[3].options.button.new_tab = 1
        i += 1;
    })
}