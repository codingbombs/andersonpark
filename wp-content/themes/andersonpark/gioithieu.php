<?php
/*
Template Name: Gioi thieu
*/
get_header();
?>


<?php
        if ( ! have_rows( 'gioithieu' ) ) {
        return false;
        }

        if ( have_rows( 'gioithieu' ) ) : ?>

<?php while ( have_rows( 'gioithieu' ) ) : the_row(); 
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $content = get_sub_field( 'content' );
        $link = get_sub_field('link');
 $imgtext = get_sub_field( 'image_text_title');

        ?>
<div class="home-section secmot gioithieu" style="background-image:url('<?php echo $bg['url']; ?>');">
    <div class="bongtop"></div>
    <div class="container-fluid">
        <div class="row  justify-content-start">

            <div class="col-md-6 col-lg-4  col-xl-4  ">
                <div class="jumbotron  bg-transparent p-0">
                    <div class="bong"></div>
                    <?php if( !empty( $imgtext ) ): ?>
                    <img class="imgtext mb-3" src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $head; ?>">
                    <?php else:  ?>
                    <h2 class="display-4 text-lux text-left line-wave mb-4"> <?php echo $head; ?></h2>
                    <?php endif; ?>


                    <p class="content text-white text-normal text-justify"> <?php echo $content; ?></p>
                    <?php 
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                
                ?>
                    <a class="btn  btn-simple p-0 align-right text-right" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button"><?php echo esc_html($link_title); ?></a>



                    <?php endif; ?>
                </div>
                <!--col -->
            </div>
            <!--jumbotron -->
        </div>

    </div>
</div>
<?php endwhile; ?>
<?php endif; ?>


<?php if( have_rows('Partners') ):  $i = 1; ?>
<div class="home-section sectam partners">
 <div class="bongtop"></div>
    <div class="container">
        <div id="row1" class="row mb-2">

            <?php while( have_rows('Partners') ): the_row(); 

		// vars
		$image = get_sub_field('logo_partner');
		$title = get_sub_field('title_partner');
		$link = get_sub_field('link_partner');
        $numrows = count( get_sub_field( 'logo_partner' ) );
         if( $i==1 || $i == 2) { 
		?>
            <div class="col p-5 text-center ani1 ">
                <h3 class="text-normal text-uppercase text-white mb-4"><?php echo $title; ?></h3>
                <a href="<?php echo $link; ?>"><img class="cdt" src="<?php echo $image['url']; ?>" /></a>
            </div>


            <?php 
          if( $i == 2) { echo '</div><div id="row2" class="row ">'; }
         } else { ?>

            <div class="col m-0 pt-5 text-center ani<?php echo $i; ?> ">
                <h5 class="text-normal text-uppercase text-white mb-3"><?php echo $title; ?></h5>
                <a href="<?php echo $link; ?>"><img class="partner" src="<?php echo $image['url']; ?>" /></a>
            </div>



            <?php } ?>
            <?php $i++; endwhile; ?>
        </div>
    </div>

</div>
<?php endif; ?>

<?php

get_footer();
