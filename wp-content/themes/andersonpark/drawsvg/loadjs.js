var ua = navigator.userAgent,
    match = ua.match("MSIE (.)"),
    versions = match && match.length > 1 ? match[1] : "unknown",
    isTouchDevice = "ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch || navigator.msMaxTouchPoints > 0 || navigator.maxTouchPoints > 0,
    isDesktop = 0 != $(window).width() && !isTouchDevice,
    IEMobile = ua.match(/IEMobile/i),
    isIE9 = /MSIE 9/i.test(ua),
    isIE10 = /MSIE 10/i.test(ua),
    isIE11 = !(!/rv:11.0/i.test(ua) || IEMobile),
    isEge = /Edge\/12./i.test(navigator.userAgent),
    isOpera = !!window.opr && !!opr.addons || !!window.opera || ua.indexOf(" OPR/") >= 0,
    isFirefox = "undefined" != typeof InstallTrigger,
    isIE = !!document.documentMode,
    isEdge = !isIE && !!window.StyleMedia && !isIE11,
    isChrome = !!window.chrome && !!window.chrome.webstore,
    isBlink = (isChrome || isOpera) && !!window.CSS,
    isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor") > 0 || !isChrome && !isOpera && void 0 !== window.webkitAudioContext,
    isSafari5 = ua.match("Safari/") && !ua.match("Chrome") && ua.match(" Version/5."),
    AndroidVersion = parseFloat(ua.slice(ua.indexOf("Android") + 8)),
    Version = ua.match(/Android\s([0-9\.]*)/i),
    isIOS8 = function () {
        var e = navigator.userAgent.toLowerCase();
        return /iphone os 8_/.test(e)
    };

function iOSversion() {
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
        var e = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(e[1], 10), parseInt(e[2], 10), parseInt(e[3] || 0, 10)]
    }
}
var ios, android, blackBerry, UCBrowser, Operamini, firefox, windows, smartphone, tablet, touchscreen, all, iOS = iOSversion(),
    isMobile = {
        ios: ua.match(/iPhone|iPad|iPod/i),
        android: ua.match(/Android/i),
        blackBerry: ua.match(/BB10|Tablet|Mobile/i),
        UCBrowser: ua.match(/UCBrowser/i),
        Operamini: ua.match(/Opera Mini/i),
        windows: ua.match(/IEMobile/i),
        smartphone: ua.match(/Android|BlackBerry|Tablet|Mobile|iPhone|iPad|iPod|Opera Mini|IEMobile/i) && window.innerWidth <= 440 && window.innerHeight <= 740,
        tablet: ua.match(/Android|BlackBerry|Tablet|Mobile|iPhone|iPad|iPod|Opera Mini|IEMobile/i) && window.innerWidth <= 1366 && window.innerHeight <= 800,
        all: ua.match(/Android|BlackBerry|Tablet|Mobile|iPhone|iPad|iPod|Opera Mini|IEMobile/i)
    };
if (isTouchDevice && null !== isMobile.all) var TouchLenght = !0;
else if (isMobile.tablet && isFirefox || isMobile.smartphone && isFirefox) TouchLenght = !0;
else TouchLenght = !1;
isMobile.Operamini && alert("Please disable Data Savings Mode");
var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
    Loadx = 0;

function changeUrl(e, t, i, a, o, s, n) {
    void 0 !== window.history.pushState && (document.URL != e && "" != e && window.history.pushState({
        path: e,
        dataName: o,
        title: t,
        keyword: a,
        description: i,
        titleog: s,
        descriptionog: n
    }, "", e));
    "" != t && ($("#hdtitle").html(t), $('meta[property="og:description"]').remove(), $("#hdtitle").after('<meta property="og:description" content="' + n + '">'), $('meta[property="og:title"]').remove(), $("#hdtitle").after('<meta property="og:title" content="' + s + '">'), $('meta[property="og:url"]').remove(), $("#hdtitle").after('<meta property="og:url" content="' + e + '">'), $("meta[name=keywords]").remove(), $("#hdtitle").after('<meta name="keywords" content="' + a + '">'), $("meta[name=description]").remove(), $("#hdtitle").after('<meta name="description" content="' + i + '">')), $("#changlanguage_redirect").val(e)
}

function ResizeWindows() {
    $(window).height(), $(window).width();
    var e, t = $(window).height() <= $(window).width(),
        i = ($(".bg-home img"), $(window).width()),
        a = $(window).height(),
        o = .5625,
        s = i / 2e3,
        n = a / 1024,
        r = i / 1800,
        l = i / 1600,
        h = i / 1350;
    a / i > o ? e = a : e = i * o, $(".show-box-pic:not(.no-pic)").each(function (e, t) {
        var i = $(t).attr("data-pic");
        $(".note-facilities li[data-text='" + i + "']").addClass("has-pic")
    }), $(".go-top").css({
        display: "none",
        opacity: 0
    }), i <= 1100 ? ($(".scrollA, .scrollB,.scrollC").getNiceScroll().remove(), $(".scrollA,  .scrollB, .scrollC").css({
        height: "auto"
    }), 1 == t ? $(".banner-home").css({
        height: e
    }) : $(".banner-home").css({
        height: "auto"
    }), 1 == t ? ($(".facilities-map").css({
        height: $(".facilities-bg").height() * r
    }), $(".facilities-bg, .all-dot").scale(r), $(".apartment-map").css({
        height: $(".apartment-bg").height() * r
    }), $(".apartment-bg").scale(r), $(".apartment-bg").css({
        left: i / 2 - 1e3,
        top: $(".apartment-map").height() / 2 - 562
    })) : ($(".facilities-map").css({
        height: $(".facilities-bg").height() * l
    }), $(".facilities-bg, .all-dot").scale(l), $(".apartment-map").css({
        height: $(".apartment-bg").height() * h
    }), $(".apartment-bg").scale(h), $(".apartment-bg").css({
        left: i / 2 - 1010,
        top: $(".apartment-map").height() / 2 - 562
    })), $(".facilities-bg, .all-dot").css({
        left: i / 2 - 1e3,
        top: $(".facilities-map").height() / 2 - 562
    }), $(".news-text img").addClass("zoom-pic")) : i > 1100 && ($(".banner-home").css({
        height: a
    }), $(".apartment-map, .facilities-map").css({
        height: a
    }), a / i > .53 ? ($(".facilities-bg, .all-dot, .apartment-bg").scale(n), $(".apartment-bg").css({
        left: i / 2 - 1e3,
        top: a / 2 - 562
    })) : ($(".facilities-bg, .all-dot, .apartment-bg").scale(s), $(".apartment-bg").css({
        left: i / 2 - 1e3,
        top: a / 2 - 600
    })), $(".facilities-bg, .all-dot").css({
        left: i / 2 - 1e3,
        top: a / 2 - 562
    }), $(".scrollB").css({
        height: a - 70
    }), $(".scrollC").css({
        height: "100%"
    }), $(".news-text img").removeClass("zoom-pic"))
}

function ScrollHoz() {
    var e = $(".news-list, .info-facilities");
    $(window).width() <= 1100 && ($(e).css({
        "overflow-x": "scroll",
        "overflow-y": "hidden",
        "-ms-touch-action": "auto",
        "-ms-overflow-style": "none",
        overflow: " -moz-scrollbars-none"
    }), $(e).animate({
        scrollLeft: "0px"
    }), 0 != TouchLenght && isTouchDevice || $(window).width() <= 1100 && ($(e).mousewheel(function (e, t) {
        e.preventDefault(), $(window).width() <= 1100 && (this.scrollLeft -= 40 * t)
    }), $(e).addClass("dragscroll"), $(".dragscroll").draptouch()))
}

function DrawLoad() {
    var e = $(".load-present"),
        t = $(e).find("path");
    $(t).each(function (e, t) {
        var i = this.getTotalLength();
        isIE9 || isIE10 || isIE11 || isEdge ? ($(this).css({
            "stroke-dasharray": i + " " + i
        }), $(this).css({
            "stroke-dashoffset": i,
            "stroke-dasharray": i + " " + i
        }), $(this).stop().animate({
            "stroke-dashoffset": 0
        }, 1e3, "linear", function () {
            $(this).stop().animate({
                "stroke-dashoffset": i
            }, 1e3, "linear")
        }), setTimeout(function () {
            $(".loadicon").addClass("show")
        }, 900)) : setTimeout(function () {
            $(".loadicon").addClass("show")
        }, 800)
    })
}

function DrawText() {
    $(".slogan").removeClass("move"), $(".slogan h2 > span > span").removeClass("move"), $(".slogan p > span > span").removeClass("move"), timer > 0 && (clearTimeout(timer), timer = 0);
    var e = $(".show-text .svg-character"),
        t = $(e).find("path");
    $(".svg-character").removeClass("active"), $(".group-central").removeClass("show"), $(t).css({
        "stroke-dasharray": "0 0"
    }), $(t).each(function (e, t) {
        var i = this.getTotalLength();
        $(this).css({
            "stroke-dasharray": i + " " + i
        }), $(this).css({
            "stroke-dashoffset": i,
            "stroke-dasharray": i + " " + i
        }), $(this).stop().animate({
            "stroke-dashoffset": 0
        }, 1500, "linear", function () {
            $(".show-text .svg-character").addClass("active"), $('.group-central[data-name="banner-home"]').hasClass("show-text") && $('.group-central[data-name="banner-home"]').addClass("show"), $("#home-page").length && $(".nav-click, .hotline").addClass("show"), addMove()
        })
    })
}

function Done() {
    $("html, body").scrollTop(0), ResizeWindows(), ScrollHoz(), SlidePicture(), $(window).width() > 1100 && $(".sub-house").length && BoxApartment(), $(".title-page > h1").lettering("words").children("span").lettering().children("span").lettering(), $(".slogan h2").lettering("words").children("span").lettering().children("span").lettering(), $(".slogan p").lettering("words").children("span").lettering().children("span").lettering(), $(".mask").addClass("hide"), $(".map-mobile").length && ($(".map-mobile").addClass("pinch-zoom"), $(".pinch-zoom").each(function (e, t) {
        new PinchZoom(t, {})
    })), $(window).width() > 1100 && ($("#facilities-page,  #apartment-page").length || BoxSlide()), $(".loadicon").stop().fadeOut(600, function () {
        ContentLoad(), $(".loadicon").removeClass("loader"), $(".loadicon").removeClass("show")
    })
}
$(document).ready(function () {
        $("html, body").scrollTop(0), ResizeWindows(), $("html, body").scrollTop(0), $(".loadicon").hasClass("loader") || ($(".loadicon").show(), $(".loadicon").addClass("loader"), DrawLoad()), $("#video-full").length && VideoFull(), $(window).width() > 1100 && $("img.map").length && $(".map").maphilight(), $(".banner-home, .pic-thumb").length && $(".banner-home, .pic-thumb").each(function (e, t) {
            var i = $(t).find("img").attr("src");
            if (i) {
                var a = i.replace(/(^url\()|(\)$|[\"\'])/g, "");
                $(t).css({
                    "background-image": "url(" + a + ")"
                })
            }
        })
    }),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], t) : t(e.jQuery)
    }(this, function (e) {
        var t, a, o, s, n, r, l, h, d, c;
        if (a = !!document.createElement("canvas").getContext, t = function () {
                var e = document.createElement("div");
                e.innerHTML = '<v:shape id="vml_flag1" adj="1" />';
                var t = e.firstChild;
                return t.style.behavior = "url(#default#VML)", !t || "object" == typeof t.adj
            }(), a || t) {
            if (a) {
                l = function (e) {
                    return Math.max(0, Math.min(parseInt(e, 16), 255))
                }, h = function (e, t) {
                    return "rgba(" + l(e.substr(0, 2)) + "," + l(e.substr(2, 2)) + "," + l(e.substr(4, 2)) + "," + t + ")"
                }, o = function (t) {
                    var i = e('<canvas style="width:' + e(t).width() + "px;height:" + e(t).height() + 'px;"></canvas>').get(0);
                    return i.getContext("2d").clearRect(0, 0, e(t).width(), e(t).height()), i
                };
                var p = function (e, t, a, o, s) {
                    if (o = o || 0, s = s || 0, e.beginPath(), "rect" == t) e.rect(a[0] + o, a[1] + s, a[2] - a[0], a[3] - a[1]);
                    else if ("poly" == t)
                        for (e.moveTo(a[0] + o, a[1] + s), i = 2; i < a.length; i += 2) e.lineTo(a[i] + o, a[i + 1] + s);
                    else "circ" == t && e.arc(a[0] + o, a[1] + s, a[2], 0, 2 * Math.PI, !1);
                    e.closePath()
                };
                s = function (t, i, a, o, s) {
                    var n = t.getContext("2d");
                    if (o.shadow) {
                        n.save(), "inside" == o.shadowPosition && n.clip();
                        var r = 100 * t.width,
                            l = 100 * t.height;
                        p(n, i, a, r, l), n.shadowOffsetX = o.shadowX - r, n.shadowOffsetY = o.shadowY - l, n.shadowBlur = o.shadowRadius, n.shadowColor = h(o.shadowColor, o.shadowOpacity);
                        var d = o.shadowFrom;
                        d || (d = "outside" == o.shadowPosition ? "fill" : "stroke"), "stroke" == d ? (n.strokeStyle = "rgba(0,0,0,1)", n.stroke()) : "fill" == d && (n.fillStyle = "rgba(0,0,0,1)", n.fill()), n.restore(), "outside" == o.shadowPosition && (n.save(), p(n, i, a), n.globalCompositeOperation = "destination-out", n.fillStyle = "rgba(0,0,0,1);", n.fill(), n.restore())
                    }
                    n.save(), p(n, i, a), o.fill && (n.fillStyle = h(o.fillColor, o.fillOpacity), n.fill()), o.stroke && (n.strokeStyle = h(o.strokeColor, o.strokeOpacity), n.lineWidth = o.strokeWidth, n.stroke()), n.restore(), o.fade && e(t).css("opacity", 0).stop().animate({
                        opacity: 1
                    }, 300, "linear")
                }, n = function (e) {
                    e.getContext("2d").clearRect(0, 0, e.width, e.height)
                }
            } else o = function (t) {
                return e('<var style="zoom:1;overflow:hidden;display:block;width:' + t.width + "px;height:" + t.height + 'px;"></var>').get(0)
            }, s = function (t, i, a, o, s) {
                var n, r, l, h;
                for (var d in a) a[d] = parseInt(a[d], 10);
                n = '<v:fill color="#' + o.fillColor + '" opacity="' + (o.fill ? o.fillOpacity : 0) + '" />', r = o.stroke ? 'strokeweight="' + o.strokeWidth + '" stroked="t" strokecolor="#' + o.strokeColor + '"' : 'stroked="f"', l = '<v:stroke opacity="' + o.strokeOpacity + '"/>', "rect" == i ? h = e('<v:rect name="' + s + '" filled="t" ' + r + ' style="zoom:1;margin:0;padding:0;display:block;position:absolute;left:' + a[0] + "px;top:" + a[1] + "px;width:" + (a[2] - a[0]) + "px;height:" + (a[3] - a[1]) + 'px;"></v:rect>') : "poly" == i ? h = e('<v:shape name="' + s + '" filled="t" ' + r + ' coordorigin="0,0" coordsize="' + t.width + "," + t.height + '" path="m ' + a[0] + "," + a[1] + " l " + a.join(",") + ' x e" style="zoom:1;margin:0;padding:0;display:block;position:absolute;top:0px;left:0px;width:' + t.width + "px;height:" + t.height + 'px;"></v:shape>') : "circ" == i && (h = e('<v:oval name="' + s + '" filled="t" ' + r + ' style="zoom:1;margin:0;padding:0;display:block;position:absolute;left:' + (a[0] - a[2]) + "px;top:" + (a[1] - a[2]) + "px;width:" + 2 * a[2] + "px;height:" + 2 * a[2] + 'px;"></v:oval>')), h.get(0).innerHTML = n + l, e(t).append(h)
            }, n = function (t) {
                var i = e("<div>" + t.innerHTML + "</div>");
                i.children("[name=highlighted]").remove(), t.innerHTML = i.html()
            };
            r = function (e) {
                var t, i = e.getAttribute("coords").split(",");
                for (t = 0; t < i.length; t++) i[t] = parseFloat(i[t]);
                return [e.getAttribute("shape").toLowerCase().substr(0, 4), i]
            }, c = function (t, i) {
                var a = e(t);
                return e.extend({}, i, !!e.metadata && a.metadata(), a.data("maphilight"))
            }, d = function (e) {
                return !!e.complete && (void 0 === e.naturalWidth || 0 !== e.naturalWidth)
            }, {};
            var m = !1;
            e.fn.maphilight = function (i) {
                return i = e.extend({}, e.fn.maphilight.defaults, i), a || m || (e(window).ready(function () {
                    document.namespaces.add("v", "urn:schemas-microsoft-com:vml");
                    var t = document.createStyleSheet();
                    e.each(["shape", "rect", "oval", "circ", "fill", "stroke", "imagedata", "group", "textbox"], function () {
                        t.addRule("v\\:" + this, "behavior: url(#default#VML); antialias:true")
                    })
                }), m = !0), this.each(function () {
                    var l, h, p, m, g, u, f;
                    if (l = e(this), !d(this)) return window.setTimeout(function () {
                        l.maphilight(i)
                    }, 200);
                    if (p = e.extend({}, i, !!e.metadata && l.metadata(), l.data("maphilight")), (f = l.get(0).getAttribute("usemap")) && (m = e('map[name="' + f.substr(1) + '"]'), l.is('img,input[type="image"]') && f && m.length > 0)) {
                        if (l.hasClass("area")) {
                            var w = l.parent();
                            l.insertBefore(w), w.remove(), e(m).unbind(".maphilight")
                        }
                        h = e('<div class="map-background"></div>').css({
                            backgroundImage: 'url("' + this.src + '")'
                        }), p.wrapClass && (!0 === p.wrapClass ? h.addClass(e(this).attr("class")) : h.addClass(p.wrapClass)), l.before(h), t && l.css("filter", "Alpha(opacity=0)"), h.append(l), (g = o(this)).height = this.height, g.width = this.width, e(m).bind("alwaysOn.maphilight", function () {
                            u && n(u), a || e(g).empty(), e(m).find("area[coords]").each(function () {
                                var e, t;
                                (t = c(this, p)).alwaysOn && (!u && a && ((u = o(l[0])).width = l[0].width, u.height = l[0].height, l.before(u)), t.fade = t.alwaysOnFade, e = r(this), s(a ? u : g, e[0], e[1], t, ""))
                            })
                        }).trigger("alwaysOn.maphilight").bind("mouseover.maphilight, focus.maphilight", function (t) {
                            var i, o, n = t.target;
                            if (!(o = c(n, p)).neverOn && !o.alwaysOn) {
                                if (i = r(n), s(g, i[0], i[1], o, "highlighted"), o.groupBy) {
                                    var l = n;
                                    (/^[a-zA-Z][\-a-zA-Z]+$/.test(o.groupBy) ? m.find("area[" + o.groupBy + '="' + e(n).attr(o.groupBy) + '"]') : m.find(o.groupBy)).each(function () {
                                        if (this != l) {
                                            var e = c(this, p);
                                            if (!e.neverOn && !e.alwaysOn) {
                                                var t = r(this);
                                                s(g, t[0], t[1], e, "highlighted")
                                            }
                                        }
                                    })
                                }
                                a || e(g).append("<v:rect></v:rect>")
                            }
                        }).bind("mouseout.maphilight, blur.maphilight", function (e) {
                            n(g)
                        }), l.before(g), l.addClass("area")
                    }
                })
            }, e.fn.maphilight.defaults = {
                fill: !0,
                fillColor: "000000",
                fillOpacity: .3,
                stroke: !0,
                strokeColor: "ffbd07",
                strokeOpacity: 1,
                strokeWidth: 10,
                fade: !0,
                alwaysOn: !1,
                neverOn: !1,
                groupBy: !1,
                wrapClass: !1,
                shadow: !0,
                shadowX: 5,
                shadowY: 5,
                shadowRadius: 10,
                shadowColor: "000000",
                shadowOpacity: .8,
                shadowPosition: "outside",
                shadowFrom: !1
            }
        } else e.fn.maphilight = function () {
            return this
        }
    });
