<?php
/*
Template Name: Homepage
*/
get_header();
?>

<div class="home-section secmot intro">
   <div class="top-head"></div>
    <div class="container-fluid">
        <div class="row">

            <!--Carousel Wrapper-->
            <div id="hometopslider" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">


                <!--Slides-->
                <div class="carousel-inner" role="listbox">
                    <?php if( have_rows('slider') ):  $i = 0; ?>
                    <?php while( have_rows('slider') ): the_row();
                        // vars
                        $image = get_sub_field('slide_image');


                        $imagetext = get_sub_field('slide_text_image');
                        $head = get_sub_field('heading_slide');
                        $content = get_sub_field('content_slide');
                        $link = get_sub_field('link_slide');

                        if ($i == 0) { $ac = 'active'; $mot = 'mot'; } else { $ac = $mot = ''; };
                        ?>
                    <!--First slide-->
                    <div id="<?php echo $mot; ?>" class="carousel-item d-md-flex <?php echo $ac; ?>" style="background: url('<?php echo $image['url']; ?>')  center center/cover no-repeat;">
                        <!--Mask-->
                        <?php if( !empty( $imagetext ) ): ?>
                        <div class="container align-self-end"> <img class="imgtext mb-3  " src="<?php echo esc_url($imagetext['url']); ?>" alt="<?php echo $head; ?>"></div>

                    <?php endif; ?>

                        <!--/Mask-->
                        <div class="bongbot"></div>
                    </div>
                    <!--/First slide-->

                    <!--/Third slide-->

                    <?php $i++; endwhile; ?>
                    <?php endif; //image ?>
                </div>
                <!--/.Slides-->

                <!--Controls-->
                <a class="carousel-control-prev" href="#hometopslider" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#hometopslider" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!--/.Controls-->
            </div>
            <!--/.Carousel Wrapper-->

        </div>
    </div>
    <div class="sb_pagination">
        <div class="sb_social">
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_twitter.png" alt="Twitter">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_facebook.png" alt="Facebook">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_instagram.png" alt="Instagram">
            </a>
        </div>
        <div class="sb_pager">
            <span class="sb_pager_current">01</span>
            <span class="sb_pager_seperator">/</span>
            <span class="sb_pager_total">08</span>
        </div>
        <div class="sb_title">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_title_lynproperty.png" alt="Lyn Property">
        </div>
    </div>

</div>
<?php
        if ( ! have_rows( 'gioithieu' ) ) {
        return false;
        }

        if ( have_rows( 'gioithieu' ) ) : ?>

<?php while ( have_rows( 'gioithieu' ) ) : the_row();
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $content = get_sub_field( 'content' );
        $link = get_sub_field('link');
        $imgtext = get_sub_field( 'image_text_title');
        ?>
<div class="home-section sechai gioithieu" >
    <div class="bongtop"></div>
    <div class="container">
        <div class="row  d-md-flex align-items-center">

            <div class="col-12 col-md-6 text-left  ">
                <div class="jumbotron  bg-transparent p-0">
                   <!-- <div class="bong"></div>-->
                    <?php if( !empty( $imgtext ) ): ?>
                    <img class="imgtext mb-3 text-center " src="<?php echo esc_url($imgtext['url']); ?>" width="443" alt="<?php echo $head; ?>">
                    <?php else:  ?>
                    <h2 class="display-4 text-lux text-left line-wave mb-4"> <?php echo $head; ?></h2>
                    <?php endif; ?>


                    <p class="content text-white text-normal text-left"> <?php echo $content; ?></p>
                         <?php	// Services Sub Repeater.
                            if ( have_rows( 'thong_so' ) ) : ?>

                    <div class="tscat row mb-4">
                        <?php
                                while ( have_rows( 'thong_so' ) ) : the_row();

                               $tents = get_sub_field( 'ten_ts' );
                                $ts = get_sub_field( 'ts' );
                                $tsphu = get_sub_field( 'tsphu' );
                                ?>

                        <div class="tschild col-md-6 bg-transparent text-white content">
                            <div class="tents"><?php echo $tents; ?></div>
                            <div class="ts"><?php echo $ts; ?></div>

                        </div>

                        <?php endwhile; ?>
                    </div>

                    <?php endif; ?>
                    <?php
                if( $link ):
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';

                ?>
                    <!--<a class="btn p-0 btn-simple text-center" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button"><span><?php echo esc_html($link_title); ?></span></a>-->



                    <?php endif; ?>
                </div>
                <!--col -->
            </div>
            <div class="col hinh" style="background:url('<?php echo $bg['url']; ?>') left center/cover no-repeat;" ></div>
            <!--jumbotron -->
        </div>
    </div>

    <div class="sb_pagination">
        <div class="sb_social">
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_twitter.png" alt="Twitter">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_facebook.png" alt="Facebook">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_instagram.png" alt="Instagram">
            </a>
        </div>
        <div class="sb_pager">
            <span class="sb_pager_current">02</span>
            <span class="sb_pager_seperator">/</span>
            <span class="sb_pager_total">08</span>
        </div>
        <div class="sb_title">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_title_gioithieu.png" alt="Gioi Thieu">
        </div>
    </div>

</div>
<?php endwhile; ?>
<?php endif; ?>


<?php //Sec vitri
        if ( ! have_rows( 'vitri' ) ) {
        return false;
        }

        if ( have_rows( 'vitri' ) ) : ?>

<?php while ( have_rows( 'vitri' ) ) : the_row();
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $imgtext = get_sub_field( 'Image_text_title');
        $content = get_sub_field( 'content' );
        $link = get_sub_field('link');

        ?>
<!--<div class="home-section secba vitri" style="background:url('<?php echo $bg['url']; ?>')  -200px center/ auto 85vh no-repeat rgb(10, 78, 115)">-->
<div class="home-section secba vitri" >
    <div class="bongtop"></div>
    <div class="container">
        <div class="row">

            <div class="d-md-flex align-items-center">


                <div class="col-md-4 col-lg-4 d-md-flex ">

                    <div class="box-vitri  bg-transparent align-self-center">
                         <div class="bong"></div>

                        <img class="imgtext mb-3" src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $head; ?>">

                        <h2 class="text-white text-left line-wave mb-4"> <?php echo $head; ?></h2>



                        <p class="content text-white text-normal text-justify "> <?php echo $content; ?></p>
                        <?php
                if( $link ):
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';

                ?>
                     <!--   <a class="btn  btn-simple p-0 align-right text-center" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button"><?php echo esc_html($link_title); ?></a>-->

                        <?php endif; ?>
                    </div>
                    <!--col -->
                </div>
                <!--jumbotron -->
                 <div id="mapimg" class="col-md-8 col-lg-8 p-0">
                    <img class="mapnew alignright" src="<?php echo $bg['url']; ?>" alt="<?php echo $head; ?>">
                    <!-- <img class="line" src="<?php echo get_stylesheet_directory_uri(); ?>/images/map/line-diamond-01.svg" alt="Vị trí độc tôn">-->


                   <!--  <img class="pointer" src="<?php echo get_stylesheet_directory_uri(); ?>/images/map/pointer-01.svg" alt="Vị trí dự án">-->
                </div>
            </div>
        </div>
    </div>

    <div class="sb_pagination">
        <div class="sb_social">
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_twitter.png" alt="Twitter">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_facebook.png" alt="Facebook">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_instagram.png" alt="Instagram">
            </a>
        </div>
        <div class="sb_pager">
            <span class="sb_pager_current">04</span>
            <span class="sb_pager_seperator">/</span>
            <span class="sb_pager_total">08</span>
        </div>
        <div class="sb_title">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_title_vitri.png" alt="Vi Tri">
        </div>
    </div>

</div>
<?php endwhile; ?>
<?php endif; ?>

<?php //Sec tien ich

        if ( ! have_rows( 'tienichmoi' ) ) {
        return false;
        }

        if ( have_rows( 'tienichmoi' ) ) : ?>

<?php while ( have_rows( 'tienichmoi' ) ) : the_row();
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $content = get_sub_field( 'content' );
        $link = get_sub_field('link');
        $imgtext = get_sub_field( 'image_text_title');
        ?>
<div class="home-section sectu tienichmoi" >

    <div class="container">
        <div class="row  d-md-flex align-items-center">
    <div class="col-md-7  text-center "  >
        <img class="img-responsive w-75" src="<?php echo $bg['url']; ?>"/>
    </div>
            <div class="col-md-5 text-left  ">
                <div class="jumbotron  bg-transparent p-0">
                   <!-- <div class="bong"></div>-->
                    <?php if( !empty( $imgtext ) ): ?>
                    <img class="imgtext mb-3 text-center " src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $head; ?>">
                    <?php else:  ?>
                    <h2 class="display-4 text-lux text-left line-wave mb-4"> <?php echo $head; ?></h2>
                    <?php endif; ?>


                    <p class="content text-white text-normal text-left"> <?php echo $content; ?></p>

                    <?php
                if( $link ):
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';

                ?>
                  <!--  <a class="btn p-0 btn-simple text-center" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button"><span><?php echo esc_html($link_title); ?></span></a>
-->


                    <?php endif; ?>
                </div>
                <!--col -->
            </div>

            <!--jumbotron -->
        </div>


    </div>

    <div class="sb_pagination">
        <div class="sb_social">
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_twitter.png" alt="Twitter">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_facebook.png" alt="Facebook">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_instagram.png" alt="Instagram">
            </a>
        </div>
        <div class="sb_pager">
            <span class="sb_pager_current">05</span>
            <span class="sb_pager_seperator">/</span>
            <span class="sb_pager_total">08</span>
        </div>
        <div class="sb_title">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_title_tienich.png" alt="Tien ich">
        </div>
    </div>

</div>
<?php endwhile; ?>
<?php endif; ?>



<?php // sec tien ich du an?>
<div class="home-section secnam matbang-custom" >
    <div class="bongtop"></div>
    <div class="section-face-content">
        <div class="face-content-inner">
            <div class="wrapper-contennt">
                <div class="face-heading">
                    <h2 class="face-title">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/face_title.png" alt="Anderson Park Mặt bằng">
                    </h2>
                    <div class="face-desc">
                        <div class="face-desc-inner">
                            Anderson Park tọa lạc ngay đại lộ Bình Dương, vị trí được so sánh như “<strong>trái tim</strong>” của thành phố Thuận An.
                        </div>
                    </div>
                    <a href="#" class="button-block"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/more_btn_01.png" alt="Xem thêm"></a>
                </div>
                <div class="face-map">
                  <!--  <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ap-mb-01.png" alt="Anderson Park">-->
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner secnam">
    <div class="carousel-item active">
      <img class="d-block img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ap-mb-01.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ap-mb-02.png" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ap-mb-03.png" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
                </div>
            </div>
        </div>
    </div>

    <div class="sb_pagination is-dark">
        <div class="sb_social">
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_twitter_b.png" alt="Twitter">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_facebook_b.png" alt="Facebook">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_instagram_b.png" alt="Instagram">
            </a>
        </div>
        <div class="sb_pager">
            <span class="sb_pager_current">06</span>
            <span class="sb_pager_seperator">/</span>
            <span class="sb_pager_total">08</span>
        </div>
        <div class="sb_title">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_title_matbang.png" alt="Mat Bang">
        </div>
    </div>

</div>


<div class="home-section secsau canho shophouse">
    <div class="shophouse-custom">
            <div>

                <?php if( have_rows('shophouse') ): ?>
                <?php while( have_rows('shophouse') ): the_row();
                            // vars
                            $images = get_sub_field('shophouse_bg');
                            $chhead = get_sub_field('shophouse_head');
                            $chcontent = get_sub_field('shophouse_content');
                            $link = get_sub_field('shophouse_link');
                            $imgtext = get_sub_field( 'Image_text_title');
                            ?>
                <?php if( $images ):    ?>
                <div class="bgslider">
                    <?php foreach( $images as $image ): ?>

                    <div class="slide " style="background:url('<?php echo $image['url']; ?>') 50% 50% no-repeat; background-size:cover;">

                    </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; //images ?>
                <div class="wrapper-content">
                    <div class="box-canho d-md-flex align-items-center ">
                        <div class="bg-transparent ">

                            <!--  <h2 class="display-4 text-white text-left ani1 animated  "> <?php echo $chhead; ?></h2>-->
                            <img class="imgtext mb-3 text-center " src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $chhead; ?>">



                                <p class="shophouse-text content  text-white text-justify ani3 animated   "> <?php echo $chcontent; ?></p>
                                <?php
                                        if( $link ):
                                            $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_self';

                                        ?>

                                <a class="ani2 animated button-block" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/more_btn_02.png" alt="Xem thêm"></a>
                                <?php endif; ?>
                            </div>
                        </div>

                        <!--col -->

                        <!--jumbotron -->

                    </div>
                </div>
                <?php endwhile; ?>


                <?php endif; //have row ?>


    <div class="sb_pagination">
        <div class="sb_social">
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_twitter.png" alt="Twitter">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_facebook.png" alt="Facebook">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_instagram.png" alt="Instagram">
            </a>
        </div>
        <div class="sb_pager">
            <span class="sb_pager_current">07</span>
            <span class="sb_pager_seperator">/</span>
            <span class="sb_pager_total">08</span>
        </div>
        <div class="sb_title">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_title_shophouse.png" alt="Shophouse">
        </div>
    </div>


    </div>
</div>

<?php

    // WP_Query arguments
    $args = array(
        'posts_per_page'         => '3',
    );

    // The Query
    $homenews = new WP_Query( $args );

?>
<div class="home-section secbay">
    <div class="bongtop"></div>
    <div class="news-custom">
        <div class="news-wrapper">
            <div class="wrapper-content">
                <div class="news-row">
                    <div class="column-left">
                        <h2 class="news-title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/news_title.png" width="271" alt="Mới nhất từ Anderson Park TIN TỨC"></h2>
                        <div class="news-desc">Cập nhật những tin tức mới nhất về dự án và thị trường</div>
                        <a href="#" class="button-block"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/more_btn_03.png" alt="Xem thêm"></a>
                    </div>
                    <div class="column-right">
                        <div class="news-slider-wrap">
                            <div class="news-slider js-news-slide">
                               <?php    // The Loop
                if ( $homenews->have_posts() ) {
                    $i = 1;
                    while ( $homenews->have_posts() ) {
                        $homenews->the_post();
                    if ( has_post_thumbnail(  $post->ID ) ) {
            ?>
                                <div class="item-slide">
                                     <a class="" href="<?php echo the_permalink();?>">
                                    <div class="item-news">
                                        <img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full'); ?>" alt="Image">
                                        <div class="caption">
                                            <p class="time">
<time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time></p>
                                            <?php the_title( '<p class="title ">', '</p>' ); ?>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                              <!--  <div class="item-slide">
                                    <div class="item-news">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/news_img_02.jpg" alt="Image">
                                        <div class="caption">
                                            <p class="time">30.11.2020</p>
                                            <p class="title">Sự giao thao giữ bình yêu và nhộn nhịn</p>
                                        </div>
                                    </div>
                                </div>-->
                                <?php
                    }
                 $i++;
                    } //end while
                }


            // Restore original Post Data
            wp_reset_postdata();
        ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sb_pagination">
        <div class="sb_social">
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_twitter.png" alt="Twitter">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_facebook.png" alt="Facebook">
            </a>
            <a href="#">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_icon_instagram.png" alt="Instagram">
            </a>
        </div>
        <div class="sb_pager">
            <span class="sb_pager_current">08</span>
            <span class="sb_pager_seperator">/</span>
            <span class="sb_pager_total">08</span>
        </div>
        <div class="sb_title">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/side_title_tintuc.png" alt="Tin Tuc">
        </div>
    </div>

</div>

<div class="home-section sectam section-partners">
 <div class="bongtop"></div>
    <div class="section-partners-inner">
        <div class="wrapper-content">
            <div class="partners-row">
                <h2 class="partners-title">PHÁT TRIỂN DỰ ÁN</h2>
                <div class="partners-logo">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_01.png" alt="Lyn property">
                </div>
                <ul class="partners-list">
                   <!-- <li>
                        <p class="text">PHÁT TRIỂN DỰ ÁN</p>
                        <div class="partners-photo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_01.png" alt="PHÁT TRIỂN DỰ ÁN"></div>
                    </li>-->
                    <li>
                        <p class="text">TỔNG THẦU</p>
                        <div class="partners-photo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_ht.png" alt="TỔNG THẦU"></div>
                    </li>
                    <li>
                        <p class="text">NGÂN HÀNG BẢO LÃNH</p>
                        <div class="partners-photo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_03.png" alt="NGÂN HÀNG BẢO LÃNH"></div>
                    </li>
                    <!--<li>
                        <p class="text"> THIẾT KẾ</p>
                        <div class="partners-photo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_04.png" alt=" THIẾT KẾ"></div>
                    </li>-->
                </ul>
            </div>
            <!--<div class="partners-slide">
                <h2 class="partners-title">SÀN LIÊN KẾT</h2>
                <div class="partners-slider-wrap">
                     <div class="partner-arrow-prev"></div>
                    <div class="partner-arrow-next"></div>
                    <div class="partners-slider js-partner">
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_05.png" alt="logo">
                        </div>
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_06.png" alt="logo">
                        </div>
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_07.png" alt="logo">
                        </div>
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_08.png" alt="logo">
                        </div>
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_09.png" alt="logo">
                        </div>
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_05.png" alt="logo">
                        </div>
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_06.png" alt="logo">
                        </div>
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_07.png" alt="logo">
                        </div>
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_08.png" alt="logo">
                        </div>
                        <div class="item-slide">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner_logo_09.png" alt="logo">
                        </div>
                    </div>
                </div>

            </div>-->
            <div class="partners-register">
                <h2 class="register-title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/text.png" alt="ĐĂNG KÝ NHẬN BÁO GIÁ"></h2>
                <p class="register-text">Bạn đang cần tư vấn chi tiết, hãy để lại thông tin bên dưới. Chúng tôi sẽ liên hệ hỗ trợ bạn sau ít phút</p>
                <form action="">
                    <div class="register-form">
                        <div class="column-01">
                            <input class="text-input" type="text" placeholder="Họ & Tên">
                        </div>
                        <div class="column-01"><input class="text-input" type="text" placeholder="Số điện thoại"></div>
                        <div class="column-02">
                            <button class="button-submit"></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="address-row">
                <h2 class="address-title">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company_name.png" alt="công ty cổ phần lyn property">
                </h2>
                <div class="address-desc">Địa chỉ: 82 Võ Văn Tần, Phường 06, Quận 3, Thành phố Hồ Chí Minh, Việt Nam<br><strong>Phone:</strong> (028) 38888 989  - <strong>Hotline:</strong> (028) 38888 989 </div>
            </div>

        </div>
    </div>

</div>


<?php


get_footer();
