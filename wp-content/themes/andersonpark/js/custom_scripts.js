$(function () {
  if ($('.js-partner').length) {
    $('.js-partner').slick({
      dots: false,
      infinite: false,
      speed: 800,
      arrows: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
          }
        },
        {
          breakpoint: 900,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 380,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    });
  }
  if ($('.js-news-slide').length) {
    $('.js-news-slide').slick({
      dots: false,
      infinite: true,
      speed: 800,
      arrows: true,
      variableWidth: true,
      responsive: [{
          breakpoint: 1370,
          settings: {
            variableWidth: false,
          }
        }
      ]
    });
  }

  $('.js-control-lienhe').click(function() {
    if (window.matchMedia('(max-width: 767px)')) {
      $('#fullpage').stop().animate({
        scrollTop: $('#fullpage')[0].scrollHeight
      }, 500);
    }
  });
});