(function ($) {
    'use strict';
    // remove animated
    /*$(document).ready(function() {

    });*/


    // fullpage
    $(document).ready(function () {
        //  if($(window).width() > 1100){
        var nav, anchors;
        var n = $(".home-section").length;
        var navmenu = false;

        if ($("body").hasClass("home")) {
            nav = ("Trang chủ, Giới thiệu, Vị trí,Tiện ích,Mặt bằng,Tin tức,Chủ đầu tư,Liên hệ").split(',');
            anchors = ("trangchu,gioithieu,vitri,tienich,matbang,tintuc,cdt,lienhe").split(',');
        } else if ($("body").hasClass("page-template-gioithieu")) {
            nav = ("Giới thiệu, Đối tác").split(',');
            anchors = ("gioithieu,doitac").split(',');
        } else if ($("body").hasClass("page-template-tienich")) {
            nav = ("Tiện ích").split(',');
            anchors = ("pagetienich").split(',');
            
        } else if ($("body").hasClass("page-template-matbangtang")) {
            navmenu = false;
            nav = ("Mặt bằng dự án, Blue Diamond, Red Diamond,Green Diamond ").split(',');
            anchors = ("matbang,blue-diamond,red-diamond,green-diamond").split(',');
        

        } else if ($("body").hasClass("page-vitri")) {
            nav = ("Vị trí, Bản đồ").split(',');
            anchors = ("vitri,bando").split(',');
        }

        var nofull = true;
        if ($("body").hasClass("nofull")) {
            nofull = false;
            // Move the paragraph from #myDiv1 to #myDiv2

        } else {
            nofull = true;
        }
        
        $('#fullpage').fullpage({

            //Navigation
            menu: '#menu',
            lockAnchors: false,
            anchors: anchors,
            navigation: navmenu,
            navigationPosition: 'LEFT',
            navigationTooltips: nav,
            showActiveTooltip: true,
            slidesNavigation: true,
            // scrollOverflow:true,
            autoScrolling: nofull,
            fixedElements: '#wrapper-navbar, #wrapper-footer',
            //Custom selectors
            sectionSelector: '.home-section',
            lazyLoading: true,
            paddingTop: '0',
            //scrollOverflow: true,
            scrollBar: false,
            normalScrollElements: '#scrollable, #footfix, .amenities, .fancybox-container',
            slideSelector: '.slide',
            verticalCentered: true,
            slidesNavigation: false,
            easing: 'easeInOutCubic',
            easingcss3: 'ease',
            resetSliders: true,
            fadingEffect: true,
            responsiveWidth: 767,
            //	showActiveTooltip: 'false',
            css3: true,
          /*  onLeave: function (index, nextIndex, direction) {
               
                 if (nextIndex == 2 ) { //index =1
                    $('.sechai').addClass('animated')
                     
                  } else
                if (nextIndex == 3 || nextIndex == 1) { //index =2
                    $('.secmot .animated').addClass('fadeOut');
                    $('.secmot .ani2.animated').removeClass('delay-3s zoomIn').addClass('zoomIn delay-500');
               
                    
                } else
                if (nextIndex == 4 || nextIndex == 2) { //index =3
                    $('.secba .animated').addClass('fadeOut delay-500');
                   
                } else
                if (nextIndex == 5 || nextIndex == 3) { //index =4
                    $('.secba .animated ').addClass('fadeOut');
                 

                } else
                if (nextIndex == 6 || nextIndex == 4) { //index =5
                    $('.sectu .animated').addClass('fadeOut');
                    $('.secnam .animated ').addClass('fadeOut');
                } else
                if (nextIndex == 7 || nextIndex == 5) { //index =6
                    $('.secnam .animated').addClass('fadeOut');
                    $('.secsau .animated ').addClass('fadeOut');
                } else
                if (nextIndex == 8 || nextIndex == 6) { //index =7
                    $('.secsau .animated ').addClass('fadeOut');
                    $('.secbay .animated ').addClass('fadeOut');
                } else
                if (nextIndex == 9 || nextIndex == 7) { //index =8
                    $('.secbay .animated ').addClass('fadeOut');
                    $('.sectam .animated ').addClass('fadeOut');

                } else
                if (nextIndex == 10 || nextIndex == 8) { //index =9
                    $('.sectam .animated').addClass('fadeOut');
                    $('.secchin .animated ').addClass('fadeOut');
                }
                if (index == n) {
                    $('.newfoot ').addClass('qhidden ');
                }
            }, //end onLeave
            afterLoad: function (anchorLink, index, destination) {


                if (index == 1) {
                    $('.secmot .ani1,.secmot .ani2, .secmot .ani3 ,.secmot .ani4').removeClass('fadeOut');
                    $('.secmot .ani1').addClass('animated fadeInDown');
                    $('.secmot .ani2').addClass('animated zoomIn');
                    $('.secmot .ani3').addClass('animated fadeInLeft');
                    $('.secmot .ani4').addClass('animated fadeInRight');
                  
                   //  $('.page-template-tienich .secmot h2').addClass('ml3');
               //  $('.sechai').removeClass('show-text');

                } else
                if (index == 2) {
                    $('.secmot .ani1,.secmot .ani2, .secmot .ani3 ').addClass('fadeOut');
                    $('.secmot .ani2').removeClass('delay-3s slow zoomIn');
                    
                   
                    $('.sechai .ani1').addClass('animated fadeInDown delay-250');
                    $('.sechai .ani2').addClass('animated fadeIn  delay-500');
                    $('.sechai .ani3').addClass('animated fadeInDown delay-750');
                    $('.secba .animated ').removeClass('fadeOut');
                } else
                if (index == 3) {
                    
                    $('.secba .ani1').addClass('animated fadeInDown delay-250');
                    $('.secba .ani2').addClass('animated fadeIn delay-500');
                    $('.secba .ani3').addClass('animated fadeInRight delay-750');
                    $('.secba .mapnew').addClass('animated fadeIn delay-500');
          
                } else
                if (index == 4) {
                    $('.secba .animated ').removeClass('fadeOut');
                    $('.secnam .animated').addClass('fadeOut');

                } else
                if (index == 5) {
                    
                    var wtl = new TimelineMax();
                    // Stroke drawSVG
                    wtl.fromTo("path", 3, {
                        drawSVG: "50% 50%"
                    }, {
                        drawSVG: "100% 0%",
                        ease: Linear.easeNone
                    });
                    // shapes fill in
                    wtl.fromTo(".wave_group", 4, {
                        fill: "none"
                    }, {
                        fill: "rgba(8, 140, 175, 0.5)"
                    });
                    wtl.fromTo(".wave_group_2", 5, {
                        fill: "none"
                    }, {
                        fill: "rgba(0, 80, 140, 0.7)"
                    }, "2");
                    wtl.totalDuration(3);
                    //  jQuery('#waveani').fadeIn().delay(2200).slideUp();     
                  
                    // $('.secnam  .ani1').addClass('animated fadeInDown');
                    setTimeout(function () {
                          $('.secnam  .ani1, .secnam  .ani2, .secnam .ani3, .secnam  .ani4 ').removeClass('animated fadeOut');
                        $('.secnam  .ani1').addClass('animated fadeInDown');
                        $('.secnam  .ani2').addClass('animated fadeInUp delay-250');
                        $('.secnam  .ani3').addClass('animated fadeInRight delay-500');
                        $('.secnam  .ani4').addClass('animated fadeInRight delay-750');
                  //      $('.sectu .animated').addClass('fadeOut');
                        $('.secsau .animated').addClass('fadeOut');
                    }, 0);

                    
                } else
                if (index == 6) {
                    var watl= new TimelineMax();
                    // Stroke drawSVG
                    watl.fromTo("path", 3, {
                        drawSVG: "50% 50%"
                    }, {
                        drawSVG: "100% 0%",
                        ease: Linear.easeNone
                    });
                    // shapes fill in
                   watl.fromTo(".wave_group", 4, {
                        fill: "none"
                    }, {
                        fill: "rgba(8, 140, 175, 0.5)"
                    });
                    watl.fromTo(".wave_group_2", 5, {
                        fill: "none"
                    }, {
                        fill: "rgba(0, 80, 140, 0.7)"
                    }, "2");
                    watl.totalDuration(4);
                    setTimeout(function () {
                         $('.secsau .ani1, .secsau .ani2, .secsau .ani3, .secsau  .ani4 ').removeClass('animated fadeOut');
                    $('.secsau  .ani1').addClass('animated fadeInDown');
                    $('.secsau  .ani2').addClass('animated fadeInUp delay-250');
                    $('.secsau  .ani3').addClass('animated fadeInLeft delay-500');
                    $('.secsau  .ani4').addClass('animated fadeInLeft delay-750');
                   // $('.secnam .animated').addClass('fadeOut');
                    $('.secbay .animated').addClass('fadeOut');
                    }, 2000);
                    
                   
                } else
                if (index == 7) {
                    $('.secbay .ani1, .secbay .ani2, .secbay .ani3, .secbay  .ani4 ').removeClass('animated fadeOut');
                    $('.secbay  .ani1').addClass('animated fadeInLeft');
                    $('.secbay  .ani2').addClass('animated fadeInUp');
                    $('.secbay  .ani3').addClass('animated fadeInDown');
                    $('.secbay  .ani4').addClass('animated fadeInRight');
                    $('.sectam .animated').addClass('fadeOut');
                } else
                if (index == 8) {
                    $('.sectam .ani1, .sectam .ani2, .sectam .ani3, .sectam .ani4, .sectam .ani5 ').removeClass('animated fadeOut');
                    $('.sectam  .ani1').addClass('animated fadeInDown');
                    $('.sectam  .ani2').addClass('animated fadeInLeft delay-250');
                    $('.sectam  .ani3').addClass('animated fadeInUp delay-500');
                    $('.sectam  .ani4').addClass('animated fadeInDown delay-750');
                    $('.sectam  .ani5').addClass('animated fadeInRight delay-750');
                    $('.secchin .animated').addClass('fadeOut');
                } else

                if (index == 9) {
                    $('.secchin .ani1, .secchin .ani2, .secchin .ani3, .secchin  .ani4 ').removeClass('animated fadeOut');
                    $('.secchin  .ani1').addClass('animated fadeInLeft');
                    $('.secchin  .ani2').addClass('animated fadeInRight');
                   // $('.secchin  .ani3').addClass('animated fadeInLeft ');
                  //  $('.secchin  .ani4').addClass('animated fadeInRight ');
                    $("fancybox").show();
                }
                if (nofull == false) {
                    $('.newfoot').appendTo($('.fullpage-wrapper'));
                }
                if ((index == n)) {
                    $('.newfoot ').removeClass('qhidden ');
                    $('.newfoot ').addClass('animated fadeInUp faster');
                }



            },
           
            afterRender: function () {
                $(".slide-tienich").slick({
                    dots: false,
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '140px',
                    variableWidth: true,
                    arrows: true,
                    slidesToScroll: 1,
                    autoplay: false,
                    adaptiveHeight: true,

                });
            },
            onSlideLeave: function( section, origin, destination, direction){
		      var leavingSlide = this;
                
                if(section.anchor == 'pagetienich'  && destination.index == 1 ) {
			// $('.secmot .slide.active .ani1,.secmot .ani2, .secmot .ani3 ').removeClass('fadeOut');
                    $('.secmot .ani1').addClass('animated fadeInDown delay-500');
                    $('.secmot  .ani2').addClass('animated fadeInUp delay-750');
                    }

            }//end afterload
          
*/
            
        });

        $('.fp-prev').html('<div class="my-arrow left"><svg width="60px" height="80px" viewbox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="45.63,75.8 0.375,38.087 45.63,0.375 "></polyline></svg></div>');
        $('.fp-next').html('<div class="my-arrow left"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60px" height="80px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="0.375,0.375 45.63,38.087 0.375,75.8"></polyline></svg></polyline></svg></div>');

        // } //end responsive
        /* if($(window).width() < 1100) {
            $('.animated').removeClass('fadeOut');
               $('.newfoot ').removeClass('qhidden ');
              $('.newfoot').appendTo( $('.fullpage-wrapper') );
        }*/
    });
    
    //end fullpage
    jQuery(document).ready(function($){
        
            $(window).scroll(function(){
                if ($(this).scrollTop() > 50) {
                    $('#go-top').fadeIn('slow');
                } else {
                    $('#go-top').fadeOut('slow');
                }
            });
            $('#go-top').click(function(){
                $("html, body").animate({ scrollTop: 0 }, 500);
                return false;
            })
    
});
})(jQuery);
