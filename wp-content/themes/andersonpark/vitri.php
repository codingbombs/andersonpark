<?php
/*
Template Name: Vi tri
*/
get_header();
?>

<?php //Sec vitri
        if ( ! have_rows( 'vitri' ) ) {
        return false;
        }

        if ( have_rows( 'vitri' ) ) : ?>

<?php while ( have_rows( 'vitri' ) ) : the_row(); 
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $content = get_sub_field( 'content' );
        $imgtext = get_sub_field( 'Image_text_title');
        ?>
<div class="home-section secba vitri" >
 <div class="bongtop"></div>
    <div class="container-fluid">
        <div class="row">

            <div class="d-md-flex align-items-center">

               
                <div class="col-md-4 col-lg-4 d-md-flex ">

                    <div class="box-vitri  bg-transparent align-self-center">
                         <div class="bong"></div>
                        <?php if( !empty( $imgtext ) ): ?>
                        <img class="imgtext mb-3" src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $head; ?>">
                        <?php else:  ?>
                        <h2 class="display-4 text-lux text-left line-wave mb-4"> <?php echo $head; ?></h2>
                        <?php endif; ?>


                        <p class="content text-white text-normal text-justify "> <?php echo $content; ?></p>
                       
                    </div>
                    <!--col -->
                </div>
                <!--jumbotron -->
                 <div id="mapimg" class="col-md-8 col-lg-8 p-0">
                    <img class="mapnew alignright" src="<?php echo $bg['url']; ?>" alt="<?php echo $head; ?>">
                    <!-- <img class="line" src="<?php echo get_stylesheet_directory_uri(); ?>/images/map/line-diamond-01.svg" alt="Vị trí độc tôn">-->
                     <svg class="map-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 1347.9 776.7"   xml:space="preserve">

                        <g>
                            <path class="st-line" d="M290.9,587.8l61.3-39.7c0.4-0.2,8-4.9,16.5-5.6c7.4-0.6,49.1-7.7,51.2-8l186.9-40.1l40.8-10.1
                                c0.2,0,18.3-3.7,29.9-12.2c12.5-9.1,76.4-47.4,79.1-49l0.5-0.2c0.2-0.1,7.3-3.5,5.9-17.6c-0.2-3.4-0.6-7.5-1.1-12.2
                                c-2.1-22.4-5.4-56.3-0.6-82.3"/>
                            <path class="st-line" d="M-8.7,539.9L23.2,428c0,0,13.1-70.5,66.7-86.7c18.2-5.2,25.8-7.5,62.2-6.1s179.4,1.1,179.4,1.1l91.5-1.7
                                c0,0,56-8.8,89.4-46.9c11.6-10.8,75.4-83,136.5-97.2C705.9,177.9,1070.7,89,1070.7,89l81.9-19.2c0,0,20.7-6.1,39.9-4
                                c16.2,0.5,40.9-3.5,61.6-15.7c20.2-17.2,53.6-48.6,53.6-48.6"/>
                            <path class="st-line" d="M245.6,547L370,399c2.4-2.9,59.2-70.3,128.8-87.1c41.2-10.5,107.6-31.8,108.3-32c0.6-0.2,1.2-0.2,1.8-0.1
                                l170.3,32.8l42.8,7.8l49.3,1.3c0.6,0,1.1,0.2,1.6,0.4l81.9,43.7c0.4,0.2,0.8,0.5,1.1,0.9c0.2,0.3,21.6,27.1,32.9,43.7
                                c10.9,16.1,28,39.4,28.9,40.5c0.8,1,8,10.7,7.5,26.4c0.1,1.1,0.1,2.2,0.1,3.3c0.3,8.5,0.6,18.1,10.9,36
                                c7.2,10.7,61.3,86.7,65.4,92.5c1.6,1.4,9.7,8.2,17.2,10.9c1.6,0.6,3.8,1.2,6.3,1.9c11,3.1,29.4,8.4,42.9,23
                                c10.5,12.1,22,22.9,46.6,23.9c0,0,0.1,0,0.1,0c24,1.9,52.4,4.8,54.8,5c2.9,0,31.7,0.9,45.1,21.8c13.6,19.6,33.4,44.5,33.6,44.7"/>
                        </g>
                        </svg>
                   <!--  <img class="pointer" src="<?php echo get_stylesheet_directory_uri(); ?>/images/map/pointer-01.svg" alt="Vị trí dự án">-->
                </div>
            </div>
        </div>

    </div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php

get_footer();
