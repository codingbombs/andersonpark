<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="format-detection", content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

		<div class="control-bar">
				<ul class="control-list">
						<li>
								<a href="#lienhe" class="js-control-lienhe">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon_01.png" alt="icon">
								</a>
						</li>
						<li>
								<a href="tel:18009099">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon_02.png" alt="icon">
								</a>
						</li>
						<li>
								<a href="#">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon_03.png" alt="icon">
								</a>
						</li>
						<li>
								<a href="#">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon_04.png" alt="icon">
								</a>
						</li>
				</ul>
		</div>

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" class="fixed-top" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav class="navbar navbar-expand-md navbar-dark bg-transparent  ">

		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						//the_custom_logo();
                    ?>
                 <div class="logo "><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand corner custom-logo-link" rel="home">
                   <?php  if (!wp_is_mobile()) { ?>
					 <img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-top.png" class="img-fluid" alt="Anderson Park">
					<?php } else {  ?>
						 <img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-top.png" class="img-fluid" alt="Anderson Park">
				<?php }  ?>

				   </a>

            </div>
					<?php } ?><!-- end custom logo -->



			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>



		</nav><!-- .site-navigation -->
               <!-- <a id="hotline" class="icon-phone  " href="tel:18009099">
                    <span class=" icon-phone text-gold d-block d-sm-none"></span>
                    <img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/hotline.svg" class="img-fluid  d-none d-lg-block" alt="1800 9099">
                </a>
           <button class=" dknt " id="trigger-overlay" ><span class="d-sm-none d-md-block">ĐĂNG KÝ NHẬN THÔNG TIN</span></button>-->
	</div><!-- #wrapper-navbar end -->

    <?php quoc_fullpage(); ?>
