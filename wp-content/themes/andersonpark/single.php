<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
  $args = array(
        'posts_per_page'         => '3',
    );

    // The Query
    $homenews = new WP_Query( $args );
?>

<div class="wrapper" id="single-wrapper">

	<div class="container" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'single' ); ?>

					<?php //understrap_post_nav(); ?>

					

				<?php endwhile; // end of the loop. ?>
     <div class=" text-center tinlienquan">

<h2 class="display-4 text-blue text-center  "> Tin liên quan</h2>
            <div class="row mb-2">

                <?php    // The Loop
                    if ( $homenews->have_posts() ) {
                        $i = 1;
                        while ( $homenews->have_posts() ) {
                            $homenews->the_post(); 
                        if ( has_post_thumbnail(  $post->ID ) ) {
                ?>


                <div class="col ">
                    <figure class=" p-2 m-1  m-2">
                        <a class="news-click" href="<?php echo the_permalink();?>"> 
                        <div class="news-thumb" style="height:200px;background:url(<?php echo get_the_post_thumbnail_url( $post->ID, 'large'); ?>) no-repeat; background-size:cover;background-position: center;"></div>
                        <figcaption>
                            <?php the_title( '<h2 class="entry-title content text-1 px-2 mb-2 pt-2 ">', '</h2>' ); ?>
                           
                        </figcaption>
</a>

                    </figure>
                </div>

                <?php    
                        }
                     $i++;
                        } //end while
                    } 


                // Restore original Post Data
                wp_reset_postdata();
            ?>

            </div>
        </div>
			</main><!-- #main -->

			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

		</div><!-- .row -->

	</div><!-- #content -->
   

</div><!-- #single-wrapper -->

<?php get_footer(); ?>
