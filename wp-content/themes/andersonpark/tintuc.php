<?php
/*
Template Name: Tin tuc
*/
get_header();
?>

<?php 

    // WP_Query arguments
    $args = array(
        'posts_per_page'         => '12',
    );

    // The Query
    $homenews = new WP_Query( $args );
?>
<div class="home-section secbay news fp-responsive" style="background:url(<?php echo get_stylesheet_directory_uri(); ?>/images/1x/bg6.png)bottom center/cover fixed no-repeat;">
 <div class="bongtop"></div>
  
    <div class="container pt-5 ">
        <div class="row mb-2 pt-5 justify-content-center">
       <h2 class="display-4 text-lux text-center line-wave mb-4 headtienich"> Tin tức</h2>
        </div>
        <div class="row mb-2 newsdetail">

            <?php    // The Loop
                if ( $homenews->have_posts() ) {
                    $i = 1;
                    while ( $homenews->have_posts() ) {
                        $homenews->the_post(); 
                    if ( has_post_thumbnail(  $post->ID ) ) {
            ?>


            <div class="col col-md-6  ani<?php echo $i; ?> animated">
                <figure class=" p-2 m-1  m-2">
                    <a class="news-click" href="<?php echo the_permalink();?>"> </a>
                    <div class="news-thumb" style="height:350px;background:url(<?php echo get_the_post_thumbnail_url( $post->ID, 'large'); ?>) no-repeat; background-size:cover;background-position: center;"></div>
                    <figcaption>
                        <?php the_title( '<h2 class="entry-title content text-1 px-2 mb-0 pt-2 text-blue">', '</h2>' ); ?>
                        <p class="text text-justify px-2 p-2 mb-0"><?php echo substr(strip_tags($post->post_content), 0, 350);?> </p>
                        
                         <p class="p-0"><a class="btn  btn-simple p-0 align-right text-right" href="<?php echo the_permalink();?>"  role="button"></a></p>
                    
                    </figcaption>


                </figure>
            </div>

            <?php    
                    }
                 $i++;
                    } //end while
                } 
            

            // Restore original Post Data
            wp_reset_postdata();
        ?>

        </div>
    </div>
    <!--<div class="wave-bottom ani2 animated"></div>-->
    
</div>
<?php

get_footer();
