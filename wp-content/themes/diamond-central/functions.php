<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );

function remove_extra_image_sizes() {
    foreach ( get_intermediate_image_sizes() as $size ) {
        if ( !in_array( $size, array(  'medium', 'medium_large', 'large' ) ) ) {
            remove_image_size( $size );
        }
    }
}
 function register_my_menus() {
  register_nav_menus(
    array(
   'primary' => __( 'Header Menu' ),
      'foot-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );
add_action('init', 'remove_extra_image_sizes');
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );
    wp_deregister_style( 'google-fonts' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 100 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();

    //wp_enqueue_style( 'animate', get_stylesheet_directory_uri() . '/css/animate.min.css', array(), $the_theme->get( 'Version' ) );
   // wp_enqueue_style( 'slick', get_stylesheet_directory_uri() . '/vendors/slick/slick.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'fullPage', get_stylesheet_directory_uri() . '/css/fullPage.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'FullscreenOverlayStyles', get_stylesheet_directory_uri() . '/vendors/FullscreenOverlayStyles/css/style1.css', array(), null);
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.css', array(), null );
    wp_enqueue_style( 'ap', get_stylesheet_directory_uri() . '/style.css', array(), null );
    if (!wp_is_mobile()) {
    wp_enqueue_style( 'animate-custom', get_stylesheet_directory_uri() . '/css/animate-custom.css', array(), null );
    } else {
         wp_enqueue_style( 'mobi', get_stylesheet_directory_uri() . '/css/mobile.css', array(), null );
    }
    
    wp_enqueue_script( 'TweenMax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js', array(), $the_theme->get( 'Version' ), false );
    wp_enqueue_script( 'drawsvg', get_stylesheet_directory_uri() . '/drawsvg/drawsvg.js', array(), $the_theme->get( 'Version' ), false );
   // wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/vendors/slick/slick.min.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'fullpage', get_stylesheet_directory_uri() . '/js/jquery.fullpage.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'fullpagecontrol', get_stylesheet_directory_uri() . '/js/fullPageok.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'modernizr', get_stylesheet_directory_uri() . '/vendors/FullscreenOverlayStyles/js/modernizr.custom.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'FullscreenOverlayJSclassie', get_stylesheet_directory_uri() . '/vendors/FullscreenOverlayStyles/js/classie.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'FullscreenOverlayJS', get_stylesheet_directory_uri() . '/vendors/FullscreenOverlayStyles/js/demo1.js', array(), $the_theme->get( 'Version' ), true );
   //    wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
   // if (is_page_template('tienich.php')) {
      //   wp_enqueue_script( 'moving-letters', 'https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js', array(), null, false );
      //   wp_enqueue_script( 'lettering', get_stylesheet_directory_uri() . '/vendors/textillate-master/assets/jquery.lettering.js', array(), $the_theme->get( 'Version' ), true );
      //   wp_enqueue_script( 'textillate', get_stylesheet_directory_uri() . '/vendors/textillate-master/jquery.textillate.js', array(), $the_theme->get( 'Version' ), true );
      //   wp_enqueue_style( 'fullPage', get_stylesheet_directory_uri() . '/css/fullPage.css', array(), $the_theme->get( 'Version' ) );
  //  }
    if (is_singular('nha-pho')) {
         wp_enqueue_script( 'fancy-js', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', array(), $the_theme->get( 'Version' ), false );
         wp_enqueue_style( 'fancy-styles', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', array(), null );
        
    }
}

add_action( 'wp_head', 'themeprefix_load_fonts' );
function themeprefix_load_fonts() {
    ?>
<!-- Code snippet to speed up Google Fonts rendering-->
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="anonymous">
<!--<link rel="preload" href="https://fonts.googleapis.com/css?family=Charm|Exo:400,400i,500,700&display=swap&subset=vietnamese" as="fetch" crossorigin="anonymous">-->
<link rel="preload" href="https://fonts.googleapis.com/css?family=Philosopher|Montserrat:300,400,700&display=swap&subset=vietnamese" as="fetch" crossorigin="anonymous">
<script type="text/javascript">
!function(e,n,t){"use strict";var o="https://fonts.googleapis.com/css?family=Philosopher|Montserrat:300,400,700&display=swap&subset=vietnamese",r="__3perf_googleFonts_4572";function c(e){(n.head||n.body).appendChild(e)}function a(){var e=n.createElement("link");e.href=o,e.rel="stylesheet",c(e)}function f(e){if(!n.getElementById(r)){var t=n.createElement("style");t.id=r,c(t)}n.getElementById(r).innerHTML=e}e.FontFace&&e.FontFace.prototype.hasOwnProperty("display")?(t[r]&&f(t[r]),fetch(o).then(function(e){return e.text()}).then(function(e){return e.replace(/@font-face {/g,"@font-face{font-display:swap;")}).then(function(e){return t[r]=e}).then(f).catch(a)):a()}(window,document,localStorage);
</script>
<!-- End of code snippet for Google Fonts -->
    <?php
}

function first_paragraph($content){
    return preg_replace('/<p([^>]+)?>/', '<p$1 class="intro-content">', $content, 1);
}
add_filter('the_content', 'first_paragraph');

/******************************************************************************************************
 * Filter the except length to 200 characters.
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
******************************************************************************************************/
function wpdocs_custom_excerpt_length( $length ) {
    return 100;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

add_filter('wpcf7_autop_or_not', '__return_false');
function my_custom_mime_types( $mimes ) {
 
// New allowed mime types.
$mimes['svg'] = 'image/svg+xml';
$mimes['svgz'] = 'image/svg+xml';
//$mimes['doc'] = 'application/msword';
 
// Optional. Remove a mime type.
unset( $mimes['exe'] );
 
return $mimes;
}
add_filter( 'upload_mimes', 'my_custom_mime_types' );

/*********
* Adding extra widgets on top of the understrap defaults
************/
if ( ! function_exists( 'draft_widgets_init' ) ) {
    /**
     * Initializes themes widgets.
     */
    function draft_widgets_init() {
        register_sidebar( array(
            'name'          => __( 'Navbar right', 'understrap' ),
            'id'            => 'navbar-right',
            'description'   => 'Widget area in the top right navbar corner',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ) );

    }
} // endif function_exists( 'understrap_widgets_init' ).
add_action( 'widgets_init', 'draft_widgets_init' );


function hs_image_editor_default_to_gd( $editors ) {
$gd_editor = 'WP_Image_Editor_GD';
$editors = array_diff( $editors, array( $gd_editor ) );
array_unshift( $editors, $gd_editor );
return $editors;
}
add_filter( 'wp_image_editors', 'hs_image_editor_default_to_gd' );

if ( ! function_exists( 'quoc_fullpage' ) ) {
  function quoc_fullpage() {
     // if ( (!(is_page('tin-tuc') ) & !is_single()) & !(is_page('mat-bang-du-an')))  {
      if ( (!(is_page('596') ) & !(is_page('tin-tuc') ) & !is_singular('post')) )  {
           echo '<div id="fullpage">';
          
        } else {
          echo '<div id="nofullpage">';
       
        } // endif 
     
  }
}

if ( ! function_exists( 'understrap_posted_on_quoc' ) ) {
	function understrap_posted_on_quoc() {
		$time_string = 'Ngày <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = 'Ngày <time class="entry-date published" datetime="%1$s">%2$s</time>';
		}
		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);
		$posted_on   = apply_filters(
			'understrap_posted_on_quoc', sprintf(
				'<span class="posted-on">%1$s <a href="%2$s" rel="bookmark">%3$s</a></span>',
				esc_html_x( '', 'post date', 'understrap' ),
				esc_url( get_permalink() ),
				apply_filters( 'understrap_posted_on_time', $time_string )
			)
		);
		
		echo $time_string ; // WPCS: XSS OK.
	}
}
add_filter('understrap_posted_on', 'understrap_posted_on_quoc');

// Register Custom Post Type
function custom_post_type_nhapho() {

	$labels = array(
		'name'                  => _x( 'Các nhà phố', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Nhà phố', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Nhà phố', 'text_domain' ),
		'name_admin_bar'        => __( 'Nhà phố', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Nhà phố', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Nhà phố', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Phố', 'text_domain' ),
		'description'           => __( 'Các khu nhà phố', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'khu-pho' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'nha-pho', $args );

}
add_action( 'init', 'custom_post_type_nhapho', 0 );
// Register Custom Taxonomy
function custom_khu_pho() {

	$labels = array(
		'name'                       => _x( 'Các khu phố', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Khu phố', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Các khu phố', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New khu phố', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'khu-pho', array( 'nha-pho' ), $args );

}
add_action( 'init', 'custom_khu_pho', 0 );