<?php
/**
 * Template Name: Mặt bằng nhà phố
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
// Exit if accessed directly.
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

while ( have_posts() ) : the_post(); ?>
<div class="home-section matbang"  style="background-color:#fff">

    <div class="container">
        <div class="row">

            <div class=" d-flex flex-column  align-items-center  w-100">

             
                   

                        <header class="entry-header align-self-center">

                            <?php the_title( '<h1 class="entry-title text-blue text-center font-weight-bold">', '</h1>' ); ?>
                            <p class="text-center text-2 "><span class="border-top pt-2 font-italic">Click chọn xem chi tiết thông tin nhà phố trên mặt bằng</span></p>
                        </header><!-- .entry-header -->


                        <div class="entry-content align-self-center" >

                            <?php the_content(); ?>


                        </div><!-- .entry-content -->

                        <footer class="entry-footer text-center align-self-center">

                       <a class="btn btn-light btn-lg active" role="button" aria-pressed="true" href="<?php echo esc_url( get_page_link( 158 ) ); ?>">Mặt bằng tổng thể</a>
                       <a class="btn btn-primary btn-lg active" role="button" aria-pressed="true" href="<?php echo esc_url( get_page_link( 687 ) ); ?>">Blue Diamond</a>
<a class="btn btn-danger btn-lg active" role="button" aria-pressed="true" href="<?php echo esc_url( get_page_link( 699 ) ); ?>">Red Diamond</a>
<a class="btn btn-success btn-lg active" role="button" aria-pressed="true" href="<?php echo esc_url( get_page_link( 701 ) ); ?>">Green Diamond</a>

                        </footer><!-- .entry-footer -->
<div class=" d-flex flex-column  justify-content-end list-inline corner-img">
    <p class="text-normal text-center font-weight-bold mb-2 text-blue">VỊ TRÍ </p>
    <?php echo get_the_post_thumbnail( $post->ID, 'medium' ); ?></div>
              
            </div>
        </div>
    </div>
</div>
<?php endwhile;

get_footer();

?>
