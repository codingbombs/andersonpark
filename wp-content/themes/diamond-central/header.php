<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">
    <?php if ( (!is_singular('can-ho')) & !is_page_template('matbang.php')) : ?>
    <div id="drawsvg_content">
      <svg id="logoani" width="350" height="310" viewBox="0 0 350 310" >
<!--<g  class="logo_group" stroke="#FBF1CD" stroke-miterlimit="10" stroke-width="1.01">-->

	<g class="logo_group" stroke="#ffffff" stroke-miterlimit="10" stroke-width="0.5" id="csd-logo">
       <g>
		<path class="st0" d="M25.7,224.4c2-0.1,3.9-0.1,5.8-0.1s3.9,0.2,6,0.6c2.1,0.4,4.2,1.2,6.3,2.2c2.1,1.1,3.8,2.5,5.3,4.2
			c1.5,1.7,2.6,4,3.5,6.8c0.9,2.8,1.3,6.2,1.3,10.2c0,4-0.7,7.6-2,10.8c-1.3,3.2-3.1,5.6-5.4,7.3c-4.4,3.3-9.8,5-16,5
			c-2.2,0-4.5-0.1-6.9-0.3h-3.3v-46.8H25.7z M25.9,270.7c0.9,0.1,2.1,0.1,3.5,0.1c1.4,0,2.5,0,3.2-0.1c0.7,0,1.7-0.2,2.9-0.6
			c1.2-0.3,2.4-0.8,3.5-1.4c1.1-0.6,2.2-1.4,3.4-2.6c1.2-1.1,2.2-2.5,3-4.1c1.9-3.7,2.9-8.4,2.9-14.3c0-4.2-0.5-7.8-1.5-10.9
			s-2.2-5.3-3.5-6.8c-1.3-1.5-2.9-2.6-4.8-3.5c-1.9-0.9-3.4-1.4-4.5-1.5c-1.1-0.1-2.4-0.2-3.9-0.2c-1.5,0-2.9,0.1-4.3,0.2V270.7z"/>
		<path class="st0" d="M67.3,225v-0.7h11.9v0.7H76v45.5h3.3v0.7H67.3v-0.7h3.1V225H67.3z"/>
		<path class="st0" d="M92.8,271.2H92l20-46.9l0.7-0.3l16.9,47.2h-5.7l-5.2-14.7H99.1L92.8,271.2z M109.9,231.6l-10.5,24.2h19.1
			L109.9,231.6z"/>
		<path class="st0" d="M143.5,271.2h-0.9l5.8-46.9h2.2l14.1,43.3l14-43.3h2.2l5.8,46.9h-5.6l-4.6-37.5l-12.1,37.5h-4.2l-12.3-37
			L143.5,271.2z"/>
		<path class="st0" d="M200.2,248.9c0-8.1,2-14.3,5.9-18.5c3.9-4.2,9-6.3,15.2-6.3c6.2,0,11.1,1.8,14.8,5.5
			c3.7,3.7,5.5,9.5,5.5,17.5s-2,14.1-5.9,18.2c-3.9,4.1-9.2,6.2-15.7,6.2c-5.7,0-10.3-1.6-13.9-4.9c-1.9-1.7-3.3-4.1-4.4-7.1
			C200.7,256.5,200.2,253,200.2,248.9z M221.1,224.8c-4.5,0-8.2,2-11.1,6c-2.9,4-4.3,9.9-4.3,17.7c0,7.8,1.4,13.4,4.1,17
			c2.7,3.6,6.3,5.4,10.9,5.4s8.2-2,11.1-6.1c2.9-4,4.3-9.9,4.3-17.6s-1.4-13.4-4.1-17C229.2,226.6,225.6,224.8,221.1,224.8z"/>
		<path class="st0" d="M256.3,271.2h-0.9v-46.9h2.3l27.9,43.3v-43.3h0.9v46.9h-4.9l-25.2-38.8V271.2z"/>
		<path class="st0" d="M307.5,224.4c2-0.1,3.9-0.1,5.8-0.1s3.9,0.2,6,0.6c2.1,0.4,4.2,1.2,6.3,2.2c2.1,1.1,3.8,2.5,5.3,4.2
			c1.5,1.7,2.6,4,3.5,6.8c0.9,2.8,1.3,6.2,1.3,10.2c0,4-0.7,7.6-2,10.8c-1.3,3.2-3.1,5.6-5.4,7.3c-4.4,3.3-9.8,5-16,5
			c-2.2,0-4.5-0.1-6.9-0.3h-3.3v-46.8H307.5z M307.7,270.7c0.9,0.1,2.1,0.1,3.5,0.1c1.4,0,2.5,0,3.2-0.1c0.7,0,1.7-0.2,2.9-0.6
			c1.2-0.3,2.4-0.8,3.5-1.4c1.1-0.6,2.2-1.4,3.4-2.6c1.2-1.1,2.2-2.5,3-4.1c1.9-3.7,2.9-8.4,2.9-14.3c0-4.2-0.5-7.8-1.5-10.9
			s-2.2-5.3-3.5-6.8c-1.3-1.5-2.9-2.6-4.8-3.5s-3.4-1.4-4.5-1.5c-1.1-0.1-2.4-0.2-3.9-0.2c-1.5,0-2.9,0.1-4.3,0.2V270.7z"/>
	</g>
	<g>
		<g>
			<path class="st1" d="M116.1,309.3c-3.3,0-5.8-0.9-7.3-2.8c-0.6-0.7-1-1.6-1.4-2.7c-0.4-1.1-0.5-2.5-0.5-4.1c0-1.6,0.3-3,0.8-4.3
				c0.5-1.3,1.2-2.2,2.1-2.9c1.7-1.3,3.9-1.9,6.3-1.9c0.7,0,1.4,0.1,2.1,0.2c0.7,0.1,1.2,0.2,1.5,0.4l0.5,0.2l-0.1,0.3
				c-1.2-0.5-2.5-0.7-4-0.7c-1.9,0-3.4,0.6-4.7,1.8c-1,0.9-1.6,2.2-2,4c-0.2,1-0.3,2.2-0.3,3.6c0,1.4,0.2,2.8,0.6,4
				c0.4,1.2,0.9,2.1,1.4,2.7c0.5,0.6,1.2,1,1.9,1.4c0.7,0.3,1.3,0.5,1.8,0.6c0.4,0.1,1,0.1,1.6,0.1c0.6,0,1.3-0.1,2.1-0.2
				c0.8-0.1,1.4-0.2,1.8-0.3l0.6-0.2l0.1,0.3C119.6,309.1,118,309.3,116.1,309.3z"/>
			<path class="st1" d="M141,290.6v0.3h-8.4v8h7.7v0.3h-7.7v9.8h8.9v0.3h-11.1v-18.6H141z"/>
			<path class="st1" d="M151.2,309.2h-0.3v-18.6h0.9l11,17.2v-17.2h0.3v18.6h-1.9l-10-15.4V309.2z"/>
			<path class="st1" d="M179,309.2h-2.2v-18.3h-4.6v-0.3h11.4v0.3H179V309.2z"/>
			<path class="st1" d="M192.9,290.9c2.1-0.1,3.9-0.2,5.4-0.2c4.4,0,6.6,1.5,6.6,4.4c0,1.2-0.3,2.3-0.9,3.3c-0.6,1-1.6,1.6-3.1,1.9
				l5.3,8.9h-2.5l-5.2-8.8h-3.4v8.8h-2.2V290.9z M198.4,290.9c-1,0-2.1,0.1-3.2,0.2v9.1h3.9c2.5-0.1,3.7-1.8,3.7-5.1
				c0-1.3-0.4-2.4-1.1-3.1C200.9,291.3,199.8,290.9,198.4,290.9z"/>
			<path class="st1" d="M214.7,309.2h-0.3l7.9-18.6l0.3-0.1l6.7,18.7h-2.3l-2.1-5.8h-7.7L214.7,309.2z M221.5,293.5l-4.2,9.6h7.6
				L221.5,293.5z"/>
			<path class="st1" d="M248.4,309.2h-10.1v-18.6h2.2v18.3h7.9V309.2z"/>
		</g>
	</g>
	<rect y="298" class="line" width="89.1" height="2.6"/>
	<rect x="260.9" y="298" class="line" width="89.1" height="2.6"/>
</g>
<g class="logo_group_2" stroke="#CDC6AB" stroke-miterlimit="2" stroke-width="0.5">
	<g>
		<polygon class="st4" points="175,0.5 191.2,23.7 175,83.8 		"/>
		<polygon class="st2" points="175,0.5 158.8,23.7 175,83.8 		"/>
	</g>
	<g>
		<polygon class="st4" points="92.6,49 120.8,46.6 164.8,90.6 		"/>
		<polygon class="st2" points="92.6,49 104.6,74.6 164.8,90.6 		"/>
	</g>
	<g>
		<polygon class="st4" points="90.9,146 103,120.5 163.1,104.4 		"/>
		<polygon class="st2" points="90.9,146 119.1,148.4 163.1,104.4 		"/>
	</g>
	<g>
		<polygon class="st4" points="175,194.6 158.8,171.4 175,111.3 		"/>
		<polygon class="st2" points="175,194.6 191.2,171.4 175,111.3 		"/>
	</g>
	<g>
		<polygon class="st4" points="259.1,146 230.9,148.4 186.9,104.4 		"/>
		<polygon class="st2" points="259.1,146 247,120.5 186.9,104.4 		"/>
	</g>
	<g>
		<polygon class="st4" points="259.1,49 247,74.6 186.9,90.6 		"/>
		<polygon class="st2" points="259.1,49 230.9,46.6 186.9,90.6 		"/>
	</g>
	<g>
		<g>
			<polygon class="st4" points="219.3,20.8 216.4,48 198.1,57.4 			"/>
		</g>
		<g>
			<polygon class="st2" points="219.3,20.8 197.1,36.8 198.1,57.4 			"/>
		</g>
	</g>
	<g>
		<g>
			<polygon class="st4" points="263.6,97.5 238.6,108.7 221.3,97.5 			"/>
		</g>
		<g>
			<polygon class="st2" points="263.6,97.5 238.6,86.3 221.3,97.5 			"/>
		</g>
	</g>
	<g>
		<g>
			<polygon class="st4" points="219.3,174.2 197.1,158.2 198.2,137.6 			"/>
		</g>
		<g>
			<polygon class="st2" points="219.3,174.2 216.5,147 198.2,137.6 			"/>
		</g>
	</g>
	<g>
		<g>
			<polygon class="st4" points="130.7,174.2 133.6,147 151.9,137.6 			"/>
		</g>
		<g>
			<polygon class="st2" points="130.7,174.2 152.9,158.2 151.9,137.6 			"/>
		</g>
	</g>
	<g>
		<g>
			<polygon class="st4" points="86.4,97.5 111.4,86.4 128.7,97.5 			"/>
		</g>
		<g>
			<polygon class="st2" points="86.4,97.5 111.4,108.7 128.7,97.6 			"/>
		</g>
	</g>
	<g>
		<g>
			<polygon class="st4" points="130.7,20.8 152.9,36.9 151.8,57.4 			"/>
		</g>
		<g>
			<polygon class="st2" points="130.7,20.8 133.5,48 151.8,57.4 			"/>
		</g>
	</g>
</g>
        </svg>
        </div>
        <div class="divblue">
        <span id="block1" class="bgblue"></span>
        <span id="block2" class="bgblue"></span>
        <span id="block3" class="bgblue"></span>
   
            </div>
    
    <script type="text/javascript">
    var tl = new TimelineMax();
    // Stroke drawSVG
    tl.fromTo("path", 2, {
        drawSVG: "50% 50%"
    }, {
        drawSVG: "100% 0%",
        ease: Linear.easeNone
    });
    // shapes fill in
    tl.fromTo(".logo_group", 2, {
        fill: "none"
    }, {
        fill: "#ffffff"
    });
     tl.fromTo(".st2", 2, {
        fill: "none"
    }, {
        fill: "#A47651"
    });
         tl.fromTo(".st4", 2, {
        fill: "none"
    }, {
        fill: "#D3AC88"
    });
         tl.fromTo(".line", 2, {
        fill: "none",
             stroke:"1"
    }, {
        fill: "#A47651",
             stroke:"0"
    });
        
   
    tl.totalDuration(2);
(function($) {
   
    jQuery('#logoani').fadeIn().delay(2200).slideUp();
    jQuery('#block1, #block3, #block5 ').fadeIn().delay(2600).animate({"top": '+=120vh'},1000);
    jQuery('#block2, #block4, #block6').fadeIn().delay(2600).animate({"bottom": '+=120vh'}, 1000);
jQuery('#drawsvg_content, .divblue').fadeIn().delay(2800).slideUp();

   // jQuery('#drawsvg_content').fadeIn().delay(2600).slideUp();
  //  jQuery('#drawsvg_content').fadeIn().delay(4500).animate({ height: 'toggle', opacity: 'toggle' }, 'slower');;
     
})();
        
</script>
<?php endif; ?>
	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" class="fixed-top" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav class="navbar navbar-expand-md navbar-dark bg-transparent  ">

		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						//the_custom_logo(); 
                    ?>
                 <div class="logo "><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand corner custom-logo-link" rel="home">
                   <?php  if (!wp_is_mobile()) { ?>
					 <img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/logo-new.svg" class="img-fluid" alt="Diamond Central">
					<?php } else {  ?>
						 <img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/logo-new.svg" class="img-fluid" alt="Diamond Central">
				<?php }  ?>
				  
				   </a>
				   
            </div>
					<?php } ?><!-- end custom logo -->

		
			
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>
           
            
      
		</nav><!-- .site-navigation -->
                <a id="hotline" class="icon-phone  " href="tel:0901801668"> 
                    <!--<span class=" icon-phone text-gold d-block d-sm-none"></span>
                    <img  src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/hotline.svg" class="img-fluid  d-none d-lg-block" alt="090.180.1668">-->
                </a>
           <button class=" dknt " id="trigger-overlay" ><span class="d-sm-none d-md-block">ĐĂNG KÝ NHẬN THÔNG TIN</span></button>
	</div><!-- #wrapper-navbar end -->
    
    <?php quoc_fullpage(); ?>
