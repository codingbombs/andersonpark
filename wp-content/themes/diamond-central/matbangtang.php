<?php
/*
Template Name: Mặt bằng tầng
*/
get_header();
?>

<?php  //Sec mat bang
        if ( ! have_rows( 'matbang' ) ) {
        return false;
        }

        if ( have_rows( 'matbang' ) ) :
        $dem = 0;
?>

<?php while ( have_rows( 'matbang' ) ) : the_row(); 
       
        $head = get_sub_field( 'tentang' );
        $content = get_sub_field( 'sc-matbang' );
       
        ?>
<div class="home-section matbang" id="mb<?php echo $dem; ?>"  style="background-color:#fff">
   
    <div class="container">
        <div class="row">
         
            <div class="d-flex justify-content-center w-100" >
              
                    <div class="box-vitri bg-transparent align-self-center">
                        <h2 class="display-4 text-white text-gold d-flex justify-content-center  "> <?php echo $head; ?></h2>
                       <button class="btn" data‑imp‑go‑to‑floor="BlueDiamond" data-imp-image-map-name="MBBlueDiamond">Blue Diamond</button>
                       <button class="btn" data‑imp‑go‑to‑floor="RedDiamond" data-imp-image-map-name="MBBlueDiamond">Red Diamond</button>

                        <div class="content text-2  text-white text-justify  py-4 d-flex justify-content-center " style="background-color:#fff"> 
                            <?php 
                             if (!wp_is_mobile()) {
                                echo do_shortcode(''.$content.'');
                                } else { ?>
                            <div class="mobile-canho  bot-dot bot_dot_mobile">
		
            <div class="box_dot">
                 <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-1">
                    <span class="circle"></span> 01 </div>
                <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>65.02 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>59.05 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct"> [Xem chi tiết]</p>
                </div>
                   
            </div>
            <div class="box_dot ">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-2">
                    <span class="circle"></span> 02 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>67.27 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>61.34 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
            <div class="box_dot ">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-3">
                    <span class="circle"></span> 03 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>90.55 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>82.82 m<sup>2</sup></span></p>
                    <p>3 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
            <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-4">
                    <span class="circle"></span> 04 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>92.68 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>84.96 m<sup>2</sup></span></p>
                    <p>3 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div> 
           <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-5">
                    <span class="circle"></span> 05 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>79.93 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>72.65 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
              <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-6">
                    <span class="circle"></span> 06 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>68.09 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>61.74 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
            <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-7">
                    <span class="circle"></span> 07 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>67.57 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>61.93 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
             <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-8">
                    <span class="circle"></span> 08 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>67.57 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>61.89 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
             <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-9">
                    <span class="circle"></span> 09 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>67.57 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>61.99 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
            <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-10">
                    <span class="circle"></span> 10 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>61.98 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>55.15 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
             <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-11">
                    <span class="circle"></span> 11 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>62.02 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>56.28 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
             <div class="box_dot"> 
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-12">
                    <span class="circle"></span> 12 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>62.78 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>57.11 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>  
            <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-13">
                    <span class="circle"></span> 13 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>71.14 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>64.46 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
            <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-14">
                    <span class="circle"></span> 14 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>71.14 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>64.43 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
             <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-15">
                    <span class="circle"></span> 15 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>62.61 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>57.52 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
             <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-16">
                    <span class="circle"></span> 16 </div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>51.30 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>46.98 m<sup>2</sup></span></p>
                    <p>1 PN - 1 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
             <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-17">
                    <span class="circle"></span> 17</div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>67.46 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>61.34 m<sup>2</sup></span></p>
                    <p>2 PN - 2 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
            <div class="box_dot">
                  <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
                <div class="zoom-box pos-18">
                    <span class="circle"></span> 18</div>
                 <div class="ct_bot_moblie">
                    <p class="ares">TT: <span>60.56 m<sup>2</sup></span></p>
                    <p class="ares">LL: <span>54.95 m<sup>2</sup></span></p>
                    <p>2 PN - 1 WC</p>
                    <p class="detail_ct">[Xem chi tiết]</p>
                </div>
                   
            </div>
        </div>
                                     
                              <?php  }
                            ?>
                           
                        </div>
                        
  
                    </div>
                    <!--col -->
               
            </div>
        </div>

    </div>
</div>
<?php  $dem++; endwhile; ?>
<?php endif; ?>

<?php

get_footer();
