<?php
/*
Template Name: Tien ich
*/
get_header();
?>

<div class="home-section secmot tienich ">
     <h2 class="display-4 text-lux text-center line-wave mb-4 headtienich"> Tiện ích hiện đại</h2>
    <?php 
        if (!wp_is_mobile()) {
       masterslider("ms-tien-ich"); 
        } else {
          masterslider("ms-tien-ich-mobi"); 
        }
    ?>
</div>
<?php

get_footer();
