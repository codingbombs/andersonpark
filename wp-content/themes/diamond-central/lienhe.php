<?php
/*
Template Name: Lien he
*/
get_header();
?>
<?php if( have_rows('lienhe') ):  ?>
<div class="home-section secchin lienhe" >
  
    <div class="container">
        <div class="row">

            <div class="col col py-3 px-lg-5 ">
               <h3 class=" text-lux text-left mb-4 mt-2"> ĐĂNG KÝ NHẬN THÔNG TIN</h3>
                <?php echo do_shortcode('[contact-form-7 id="134"]');?>
            </div>
            <div class="col text-lux col py-3 px-lg-5">
                <?php while( have_rows('lienhe') ): the_row(); 
		// vars
		$tenvp = get_sub_field('ten_vp');
		$diachi = get_sub_field('diachi');
		$email = get_sub_field('email_vp');
		$dt = get_sub_field('dt_vp');
        if ($tenvp):
		?>
                <div class="box-info">
                <h3 class="heading text-uppercase  mt-2 "><?php echo $tenvp; ?></h3>
                <p class="p-0 m-0"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $diachi; ?></p>
                <?php if ($dt):  ?><p><i class="fa fa-envelope"></i> <?php echo $dt; ?></p> <?php endif; ?>
                <?php if ($email):  ?><p><i class="fa fa-phone-square"></i> <?php echo $email; ?></p><?php endif; ?>
                    <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
      <!--  <div class="wave-bottom dark ani3 animated"></div>-->
    </div>
    </div>
    <?php endif; ?>
<?php

get_footer();
