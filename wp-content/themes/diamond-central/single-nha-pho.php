<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
  $args = array(
        'posts_per_page'         => '3',
    );

    // The Query
    $homenews = new WP_Query( $args );
$i = $j = 0;
?>

<div class="wrapper" id="maincanho">

    <div class="container-fluid" id="content" tabindex="-1">



        <main class="site-main ani" id="main">

            <?php while ( have_posts() ) : the_post(); 
            
             $vitri = get_field('vitri');
            ?>

            <article class="row " id="post-<?php the_ID(); ?>">
                <div class=" col-md-8 d-flex flex-column justify-content-around">
                    <header class="entry-header ani2"> 
                        <?php the_title( '<h1 class="entry-title text-blue text-center font-weight-bold">', '</h1>' ); ?> 
                    <p class="text-center text-2 "><span class="border-top pt-2">Diện tích: <?php the_field('dientich'); ?> m2</span></p>
                    </header><!-- .entry-header -->
                    

                    <div class="entry-content">
                        <!--Carousel Wrapper-->
                        <div id="nhaslider" class="carousel  carousel-fade">


                            <!--Slides-->
                            <div class="carousel-inner" role="listbox">
                                <?php if( have_rows('floor-details') ): 
                                 $tieude = $dtcactang = '';  
                                $t = 0;
                                ?>
                                <?php while( have_rows('floor-details') ): the_row(); 

                                                    // Get sub field values.
                                                   
                                                   // $thumb = wp_get_attachment_image_src(get_sub_field('hinh-mbt'), 'medium' ); 
                                                   // $image = wp_get_attachment_image_src(get_sub_field('hinh-mbt'), 'full'); 
                                                    $tentang = get_sub_field('tentang');
                                                    $dttang = get_sub_field('dttang');
                                                    $a = '';
                                                    if (($i == 0) || ($j = 0)) { $a = 'active'; }  
                                         
                                                    $tieude .= '<li class="'.$a.'" data-target="#nhaslider" data-slide-to="'.$t.'">'.$tentang.'</li>';
                                                    $dtcactang .= '<li class="list-inline-item" >'.$tentang.': <strong>'.$dttang.'</strong> m<sup>2</sup></li>';
                                                             // Image variables.
                                                    $image = get_sub_field('hinh-mbt');
                                                        if( $image ):
                                                        $url = $image['url'];
                                                        $title = $image['title'];
                                                        $alt = $image['alt'];
                                                        $caption = $image['caption'];

                                                        // Thumbnail size attributes.
                                                        $size = 'large';
                                                        $thumb = $image['sizes'][ $size ];
                                                        $width = $image['sizes'][ $size . '-width' ];
                                                        $height = $image['sizes'][ $size . '-height' ];
                                                        endif;
                                                    ?>

                                <div class="carousel-item <?php echo $a; ?>">

                                    <a class="thumbnail fancybox" data-src="#hidden-content" href="javascript:;" data-fancybox="gallery" rel="ligthbox" title="<?php echo esc_attr($title); ?>">
                                        <div class="nha-content d-flex justify-content-around " id="hidden-content">
                                            <?php the_title( '<h1 class="entry-title text-blue text-center font-weight-bold">', '</h1>' ); ?>
                                             <p class="text-center text-2 "><span class="text-center border-top pt-2">Diện tích: <?php the_field('dientich'); ?> m2</span></p>
                                            <div class="d-block w-100" style="background: url('<?php echo esc_url($thumb); ?>') center center/contain no-repeat"></div>
                                            <!--<img  style="height:360px" src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" />-->
                                            <div class="carousel-caption d-none d-md-block text-normal text-left ">

                                                <ul class="list-inline">

                                                    <li class="list-inline-item text-2 text-blue"><strong><?php the_sub_field('tentang'); ?></strong></li>
                                                    <li class="list-inline-item text-2 "><strong><?php the_sub_field('dttang'); ?></strong> m<sup>2</sup></li>
                                                    <ul class="list-inline">
                                                        <?php if( have_rows('dtphong') ): ?>
                                                        <?php while( have_rows('dtphong') ): the_row(); 

                                                    // Get sub field values.
                                                    $hinhmbt = get_sub_field('tenthongso');
                                                    $tentang = get_sub_field('giatri');

                                                    ?>

                                                        <li class="list-inline-item "><?php echo $hinhmbt;  ?>: <?php echo $tentang; ?>m<sup>2</sup></li>


                                                        <?php  $i++;   endwhile; ?>

                                                        <?php   else: ?>
                                                     
                                                        
                                                        <li class="list-inline-item">&nbsp;</li>
                                                        <?php endif;   ?>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>

                                    </a>

                                </div>
                                <?php //the_content(); ?>
                                <?php $t++; endwhile; ?>

                                <?php endif; ?>

                            </div><!-- .entry-content -->

                            <!--Controls-->
                            <a class="carousel-control-prev text-lux justify-content-start" href="#nhaslider" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"><svg width="30px" height="60px" viewbox="0 0 50 100" xml:space="preserve">
                                        <polyline fill="none" stroke="#1E3936" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="45.63,75.8 0.375,38.087 45.63,0.375 "></polyline>
                                    </svg></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next text-lux justify-content-center" href="#nhaslider" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="60px" viewBox="0 0 50 100" xml:space="preserve">
                                        <polyline fill="none" stroke="#1E3936" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="0.375,0.375 45.63,38.087 0.375,75.8"></polyline>
                                    </svg></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!--/.Controls-->
                            <ol class="carousel-indicators">

                                <?php echo $tieude; ?>
                            </ol>
                        </div>


                    </div><!-- .entry-content -->
                    <?php /*
                    <div class="d-flex flex-column text-normal noted">
                        <H3 class="text-2 font-weight-bold text-blue">THÔNG SỐ</H3>
                        <ul class="list-inline text-3">
                            <li>Diện tích: <?php the_field('dientich'); ?> m2 </li>
                            <?php echo $dtcactang; ?>

                        </ul>
                    </div> */ ?>
                </div><!-- .entry-content -->
                <div class=" col-md-4 d-flex flex-column justify-content-around ">
                 
                     <a class=" text-right text-2 font-weight-bold text-blue py-2 w-100 xemcanho" href="#"><span class="head">XEM CĂN KHÁC</span></a>
                    <div class="noithat-slider d-flex align-items-center align-self-center w-100 p-2 justify-content-center flex-column ">
                        <h3 class="text-2 font-weight-bold text-blue py-2 w-100"><span class="head">ALBUM NỘI THẤT</span></h3>
                    <?php     echo do_shortcode('[masterslider alias="ms-noithat-tom"]'); ?>
                    </div>
                    <footer class="entry-footer align-self-end align-items-end flex-column ">
                        <h3 class="text-2 font-weight-bold text-blue py-2 w-100"><span class="head">VỊ TRÍ</span></h3>
                        <img class="w-75" src="<?php echo esc_url( $vitri['url'] ); ?>" alt="<?php echo esc_attr( $vitri['alt'] ); ?>" />

                    </footer><!-- .entry-footer -->
                </div>
            </article><!-- #post-## -->

            <?php endwhile; // end of the loop. ?>
           
        </main><!-- #main -->



    </div><!-- #content -->


</div><!-- #single-wrapper -->

<?php get_footer(); ?>
