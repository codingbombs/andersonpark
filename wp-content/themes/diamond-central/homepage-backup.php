<?php
/*
Template Name: Homepage Backup
*/
get_header();
?>

<?php /*

<div class="home-section secmot ">
<!--    <button class="my-arrow left">
            <svg width="60px" height="80px" viewbox="0 0 50 80" xml:space="preserve">
                <polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="
                    45.63,75.8 0.375,38.087 45.63,0.375 ">
            </polyline></svg>
        </button>
        <button class="my-arrow right">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60px" height="80px" viewbox="0 0 50 80" xml:space="preserve">
                <polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="
                    0.375,0.375 45.63,38.087 0.375,75.8 ">
            </polyline></svg>
        </button>-->
    <div class="container-fluid">
        <div class="row">
            <div class="logo">
                <!--<a href="http://asianaluxury.vn/" class="navbar-brand custom-logo-link" rel="home"><img width="180" height="140" src="http://asianaluxury.vn/wp-content/uploads/2019/06/logo.png" class="img-fluid" alt="Asiana Luxury"></a>-->
                <!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

<?php if ( is_front_page() && is_home() ) : ?>

<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

<?php else : ?>

<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

<?php endif; ?>


<?php } else {
						the_custom_logo();
					} ?>
<!-- end custom logo -->
</div>
<?php if( have_rows('slider') ): ?>
<?php while( have_rows('slider') ): the_row(); 
                        // vars
                        $image = get_sub_field('slide_image');
                        $imagetext = get_sub_field('slide_text_image');
                        $head = get_sub_field('heading_slide');
                        $content = get_sub_field('content_slide');
                        $link = get_sub_field('link_slide');

                        ?>
<?php if( $image ): ?>
<div class="slide animatedParent " style="background:url('<?php echo $image['url']; ?>') no-repeat; background-size:cover;">
    <?php if( $image ): ?>
    <div class="offset-md-2 col-md-4 alignleft">
        <img style="max-height: 70vh;" class="image-text img-responsive  ani2 animated" src="<?php echo $imagetext['url']; ?>" />
    </div>
    <?php else : //image ?>
    <h2 class="display-3 text-white text-center ani1 animated  "> <?php echo $head; ?></h2>
    <h3 class="content  text-center ani2 animated   "> <?php echo $content; ?></h3>
    <?php endif; //image ?>
</div>
<?php endif; //image ?>
<?php endwhile; ?>


<?php endif; //have row ?>
<div class="mouse-down animated ani3 ">
    <a href="#gioithieu" class="animated ani3  infinite">
        <span class="ic mouse"></span>
    </a>
</div>
</div>
</div>
</div>
*/ ?>
<div class="home-section secmot ">
    <div class="container-fluid">
        <div class="row">
            <div class="logo">

                <?php if ( ! has_custom_logo() ) { ?>

                <?php if ( is_front_page() && is_home() ) : ?>

                <h1 class="navbar-brand mb-0 corner"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

                <?php else : ?>

                <a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

                <?php endif; ?>


                <?php } else { 
						//the_custom_logo(); ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand corner custom-logo-link" rel="home">
                    <img width="180" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-fluid" alt="Asiana Luxury"></a>
            
				<?php	} ?>
                <!-- end custom logo -->
            </div>
            <!--Carousel Wrapper-->
            <div id="hometopslider" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
        <!--Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-2" data-slide-to="1" ></li>
       
        </ol>
        <!--/.Indicators-->

        <!--Slides-->
        <div class="carousel-inner" role="listbox">
<?php if( have_rows('slider') ):  $i = 0; ?>
<?php while( have_rows('slider') ): the_row(); 
                        // vars
                        $image = get_sub_field('slide_image');
                      
   
                        $imagetext = get_sub_field('slide_text_image');
                        $head = get_sub_field('heading_slide');
                        $content = get_sub_field('content_slide');
                        $link = get_sub_field('link_slide');
            
                        if ($i == 0) { $ac = 'active'; } else { $ac = ''; };
                        ?>
            <!--First slide-->
            <div class="carousel-item <?php echo $ac; ?>">
                <!--Mask-->
                <div class="view">
                  <!--Video source-->
                <?php    $linkvideo = get_sub_field('slide_video'); 
                    if ($linkvideo) :
                    ?>
                  <video data-autoplay loop playsinline muted>
                      <source src="<?php echo esc_url($linkvideo); ?>" type="video/mp4" />
                  </video>
                    <?php endif; ?>
                  <!-- Carousel content -->
                  <div class="full-bg-img flex-center mask white-text ">
                    <img style="max-height: 50vh;" class="image-text img-responsive ani2  animated  delay-3s" src="<?php echo $imagetext['url']; ?>" />
                  </div>
                </div>
                <!--/Mask-->
            </div>
            <!--/First slide-->
         
            <!--/Third slide-->
         
<?php $i++; endwhile; ?>
               <?php endif; //image ?>
        </div>
        <!--/.Slides-->

        <!--Controls-->
        <a class="carousel-control-prev" href="#hometopslider" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#hometopslider" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
           
        </div>
    </div>
</div>
<?php
        if ( ! have_rows( 'gioithieu' ) ) {
        return false;
        }

        if ( have_rows( 'gioithieu' ) ) : ?>

<?php while ( have_rows( 'gioithieu' ) ) : the_row(); 
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $content = get_sub_field( 'content' );
        $link = get_sub_field('link');
        $imgtext = get_sub_field( 'image_text_title');
        ?>
<div class="home-section sechai gioithieu" style="background-image:url('<?php echo $bg['url']; ?>');">
    <div class="container-fluid">
        <div class="row">
            <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand corner custom-logo-link" rel="home">
                    <img width="180" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-fluid" alt="Asiana Luxury"></a>
            </div>
            <div class="col-md-6 col-lg-4   col-xl-4  offset-md-1 p-0 offset-lg-1">
                <div class="jumbotron bg-transparent p-0">
                    <?php if( !empty( $imgtext ) ): ?>
                    <img class="imgtext" src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $head; ?>">
                    <?php else:  ?>
                    <h2 class="display-4 text-lux text-gold text-left ani1 animated line-wave"> <?php echo $head; ?></h2>
                    <?php endif; ?>


                    <?php	// Services Sub Repeater.
                            if ( have_rows( 'thong_so' ) ) : ?>

                    <ul class="list-group list-group-horizontal-md list-group-horizontal-lg  thongso ani3 animated ">
                        <?php
                                while ( have_rows( 'thong_so' ) ) : the_row();

                              //  $tents = get_sub_field( 'ten_ts' );
                                $ts = get_sub_field( 'ts' );
                                $tsphu = get_sub_field( 'tsphu' );
                                ?>

                        <li class="list-group-item bg-transparent text-white content ">
                            <!--   <div class="tents"><?php echo $tents; ?></div>-->
                            <div class="ts"><?php echo $ts; ?></div>
                            <div class="tsphu"><?php echo $tsphu; ?></div>
                        </li>

                        <?php endwhile; ?>
                    </ul>

                    <?php endif; ?>
                    <p class="content  text-2 text-white text-justify ani4 animated   "> <?php echo $content; ?></p>
                    <?php 
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                
                ?>
                    <!-- <a class="btn  btn-nude ani4 animated " href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button"><?php echo esc_html($link_title); ?></a>-->
                    <a class="ani4 animated alignleft btn-xemthem" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button">

                    </a>
                    <?php endif; ?>
                </div>
                <!--col -->
            </div>
            <!--jumbotron -->
        </div>

    </div>
</div>
<?php endwhile; ?>
<?php endif; ?>


<?php //Sec vitri
        if ( ! have_rows( 'vitri' ) ) {
        return false;
        }

        if ( have_rows( 'vitri' ) ) : ?>

<?php while ( have_rows( 'vitri' ) ) : the_row(); 
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $imgtext = get_sub_field( 'Image_text_title');
        $content = get_sub_field( 'content' );
        $link = get_sub_field('link');

        ?>
<!--<div class="home-section secba vitri" style="background:url('<?php echo $bg['url']; ?>')  -200px center/ auto 85vh no-repeat rgb(10, 78, 115)">-->
<div class="home-section secba vitri" style="background:rgb(10, 78, 115)">
    <div id="map" class=" ani4 animated" style="background-image: url('<?php echo $bg['url']; ?>')"></div>
    
    <div class="container-fluid">
        <div class="row">
 <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand custom-logo-link corner" rel="home">
                    <img width="180" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-fluid" alt="Asiana Luxury"></a>
            </div>
            <div class="d-flex justify-content-end">

                <div class="col-md-6 col-lg-4 p-0 d-flex">
                    
                    <div class="box-vitri bg-transparent align-self-center">
                        <?php if( !empty( $imgtext ) ): ?>
                        <img class="imgtext ani1 animated " src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $head; ?>">
                        <?php else:  ?>
                        <h2 class="display-4 text-white text-gold text-left ani1 animated line-wave "> <?php echo $head; ?></h2>
                        <?php endif; ?>


                        <p class="content text-2  text-white text-justify ani3 animated   "> <?php echo $content; ?></p>
                                    <?php 
                                        if( $link ): 
                                            $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_self';

                                        ?>
                    <!-- <a class="btn  btn-nude ani4 animated " href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button"><?php echo esc_html($link_title); ?></a>-->
                    <a class="ani2 animated alignleft btn-xemthem" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button">

                    </a>
                    <?php endif; ?>
                    </div>
                    <!--col -->
                </div>
                <!--jumbotron -->
            </div>
        </div>

    </div>
</div>
<?php endwhile; ?>
<?php endif; ?>

<?php // sec tien ich du an?>
<div class="home-section sectu tienich " style="background:url(<?php echo get_stylesheet_directory_uri(); ?>/images/bg-keyvisual.jpg) no-repeat; background-size:cover;">
     <img class="imgtext ani1 animated headtienich" src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/tienichduan.svg">
    <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand corner custom-logo-link" rel="home">
            <img width="180" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-fluid" alt="Asiana Luxury"></a>
        
    </div>
   
    <?php masterslider("ms-tien-ich"); ?>
    <?php// masterslider(2); ?>
   <?php /* <div class="container-fluid">

        <div class="row ">

            <div class="col">


                <img class="imgtext ani1 animated" src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/tienichduan.svg">
                <div class="slide-tienich ">

                    <?php if( have_rows('tienich') ): ?>
                    <?php while( have_rows('tienich') ): the_row(); 
                                    // vars
                                    $images = get_sub_field('tienich_image');
                                    $tenti = get_sub_field('ten_ti');
                                    $link = get_sub_field('link_ti');

                                    ?>
                    <?php if( $images ): ?>
                    <div class="box-image" style="background-image:url('<?php echo $images['url']; ?>');">
                        <!--<img alt="" src="<?php echo $images['url']; ?>"/>-->
                        <h3 class="content text-white text-center"> <?php echo $tenti; ?></h3>
                    </div>
                    <?php endif; //image ?>
                    <?php endwhile; ?>


                    <?php endif; //have row ?>

                </div>

            </div>

        </div>

    </div> 
    <!--  <div class="wave-bottom ani2 animated"></div>-->
    <svg class="ani2 animated" viewBox="0 0 120 18" id="wavebot">

        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="1" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="
           1 0 0 0 0  
           0 1 0 0 0  
           0 0 1 0 0  
           0 0 0 13 -9" result="goo" />
                <xfeBlend in="SourceGraphic" in2="goo" />

            </filter>
            <linearGradient id="grad2" x1="0%" y1="0%" x2="0%" y2="100%">
                <stop offset="0%" style="stop-color:#4CBBF2;stop-opacity:1" />
                <stop offset="50%" style="stop-color:#07629B;stop-opacity:1" />
                <stop offset="100%" style="stop-color:#004772;stop-opacity:1" />
            </linearGradient>
            <path id="wave" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" />
        </defs>
        <use id="wave1" class="wave" xlink:href="#wave" fill="url(#grad2)" x="0" y="-4"></use>
        <use id="wave3" class="wave" xlink:href="#wave" fill="url(#grad2)" x="0" y="-2"></use>
        <use id="wave2" class="wave" xlink:href="#wave" fill="url(#grad2)" x="0" y="0"></use>
        <use id="wave4" class="wave" xlink:href="#wave" fill="url(#grad2)" x="0" y="2"></use>

    </svg>*/ ?>
</div>
<div class="home-section secnam canho ">




    <div class="container-fluid">
        <div class="row">
            <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand custom-logo-link corner" rel="home">
                    <img width="180" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-fluid" alt="Asiana Luxury"></a>
            </div>
            <?php if( have_rows('canho') ): ?>
            <?php while( have_rows('canho') ): the_row(); 
                        // vars
                        $images = get_sub_field('canho_bg');
                        $chhead = get_sub_field('canho_head');
                        $chcontent = get_sub_field('canho_content');
                        $link = get_sub_field('canho_link');

                        ?>
            <?php masterslider(4); ?>
            <?php /* if( $images ):  
            <div class="bgslider">
                <?php foreach( $images as $image ): ?>

                <div class="slide " style="background:url('<?php echo $image['url']; ?>') no-repeat; background-size:cover;">

                </div>
                <?php endforeach; ?>
            </div>
            endif; //images */ ?>

            <div class="box-canho ">
                <div class="row bg-transparent ">
                    <div class="col p-0">
                       <!-- <h2 class="display-4 text-white text-left ani1 animated  "> <?php echo $chhead; ?></h2>-->
                         <img class="imgtext ani1 animated headtienich" src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/canhoviewbien.svg">
                       
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12 pt-3">
                         <?php	/*   // Services Sub Repeater.
                            if ( have_rows( 'canho_thongso' ) ) : ?>
                        <ul class="list-group list-group-horizontal-md list-group-horizontal-lg  thongso">
                            <?php
                                            while ( have_rows( 'canho_thongso' ) ) : the_row();

                                            $tents = get_sub_field( 'ten_ts_ch' );
                                            $ts = get_sub_field( 'ts_ch' );
                                            $tsphu = get_sub_field( 'tsphu_ch' );
                                            ?>

                            <li class="list-group-item bg-transparent text-white content ani2 animated ">
                                <div class="tents"><?php echo $tents; ?></div>
                                <div class="ts"><?php echo $ts; ?></div>
                                <div class="tsphu"><?php echo $tsphu; ?></div>
                            </li>

                            <?php endwhile; ?>
                        </ul>
                        <?php endif; //canho_th */ ?>
                        <p class="content text-2  text-white text-justify ani3 animated   "> <?php echo $chcontent; ?></p>
                        <?php 
                                if( $link ): 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';

                                ?>
                       
                         <a class="ani2 animated alignleft btn-xemthem" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button">

                    </a>
                        <?php endif; ?>
                    </div>
                </div>
                <!--col -->

                <!--jumbotron -->
                <div id="drawsvg_wave">
                    <svg id="waveani" width="1043" height="253" viewBox="0 0 1043 253">
                        <path class="wave_group" stroke="#088CAF" stroke-miterlimit="10" stroke-width="0.5" d="M11,253c0,0,1.16-108.14,50-166c24.38-28.89,70-40,103-28c17.95,6.53,121,48,162,29s80-58,122-35s93,52,142,12
	s115,13,162,13c72,0,142-38,208-38c55,0,67,24,83,50c0,36,0,163,0,163H11z" />
                        <path class="wave_group_2" stroke="#00508C" stroke-miterlimit="10" stroke-width="0.5" d="M21,253c0,0,7-76,25-112s36-75,86-74s78,26,105,26c50,7,53-39,113-39c50,0,49,32,107,35s56-36,120-46
	s103,46,160,48s63.46-31.27,151-51c71-16,137,52,155,75c0,31,0,138,0,138H21z" />

                    </svg>
                </div>
            </div>
            <?php endwhile; ?>


            <?php endif; //have row ?>

        </div>
    </div>
</div>

<div class="home-section secsau canho shophouse">
    <div class="container-fluid">
        <div class="row">
            <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand custom-logo-link corner" rel="home">
                    <img width="180" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-fluid" alt="Asiana Luxury"></a>
            </div>
            <?php if( have_rows('shophouse') ): ?>
            <?php while( have_rows('shophouse') ): the_row(); 
                        // vars
                        $images = get_sub_field('shophouse_bg');
                        $chhead = get_sub_field('shophouse_head');
                        $chcontent = get_sub_field('shophouse_content');
                        $link = get_sub_field('shophouse_link');

                        ?>
            <?php if( $images ):    ?>
            <div class="bgslider">
                <?php foreach( $images as $image ): ?>

                <div class="slide " style="background:url('<?php echo $image['url']; ?>') no-repeat; background-size:cover;">

                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; //images ?>

            <div class="box-canho ">
                <div class="row bg-transparent ">
                    <div class="col p-0">
                      <!--  <h2 class="display-4 text-white text-left ani1 animated  "> <?php echo $chhead; ?></h2>-->
                          <img class="imgtext ani1 animated headtienich" src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/shophousehiendai.svg">
                        <?php	/* // Services Sub Repeater.
                            if ( have_rows( 'shophouse_thongso' ) ) : ?>
                        <ul class="list-group list-group-horizontal-md list-group-horizontal-lg  thongso">
                            <?php
                                            while ( have_rows( 'shophouse_thongso' ) ) : the_row();

                                            $tents = get_sub_field( 'ten_ts_sh' );
                                            $ts = get_sub_field( 'ts_sh' );
                                            $tsphu = get_sub_field( 'tsphu_sh' );
                                            ?>

                            <li class="list-group-item bg-transparent text-white content ani2 animated ">
                                <div class="tents"><?php echo $tents; ?></div>
                                <div class="ts"><?php echo $ts; ?></div>
                                <div class="tsphu"><?php echo $tsphu; ?></div>
                            </li>

                            <?php endwhile; ?>
                        </ul>
                        <?php endif; //canho_th */ ?>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12 pt-3">
                        <p class="content text-2  text-white text-justify ani3 animated   "> <?php echo $chcontent; ?></p>
                        <?php 
                                if( $link ): 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';

                                ?>
                       
                         <a class="ani2 animated alignleft btn-xemthem" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button">

                    </a>
                        <?php endif; ?>
                    </div>
                </div>
                <!--col -->

                <!--jumbotron -->
                <div id="drawsvg_wave">
                    <svg id="waveani" width="1043" height="253" viewBox="0 0 1043 253">
                        <path class="wave_group" stroke="#088CAF" stroke-miterlimit="10" stroke-width="0.5" d="M11,253c0,0,1.16-108.14,50-166c24.38-28.89,70-40,103-28c17.95,6.53,121,48,162,29s80-58,122-35s93,52,142,12
	s115,13,162,13c72,0,142-38,208-38c55,0,67,24,83,50c0,36,0,163,0,163H11z" />
                        <path class="wave_group_2" stroke="#00508C" stroke-miterlimit="10" stroke-width="0.5" d="M21,253c0,0,7-76,25-112s36-75,86-74s78,26,105,26c50,7,53-39,113-39c50,0,49,32,107,35s56-36,120-46
	s103,46,160,48s63.46-31.27,151-51c71-16,137,52,155,75c0,31,0,138,0,138H21z" />

                    </svg>
                </div>
            </div>
            <?php endwhile; ?>


            <?php endif; //have row ?>

        </div>
    </div>
</div>
<?php 

    // WP_Query arguments
    $args = array(
        'posts_per_page'         => '4',
    );

    // The Query
    $homenews = new WP_Query( $args );
?>
<div class="home-section secbay news" style="background:url(<?php echo get_stylesheet_directory_uri(); ?>/images/newsbg.jpg) no-repeat; background-size:cover;">
    <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand custom-logo-link corner" rel="home">
            <img width="180" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-fluid" alt="Asiana Luxury"></a>
    </div>
    <div class="container  text-center">

       <img class="imgtext ani1 animated  headtintuc" src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/tintuc.svg">
        <div class="row mb-2">

            <?php    // The Loop
                if ( $homenews->have_posts() ) {
                    $i = 1;
                    while ( $homenews->have_posts() ) {
                        $homenews->the_post(); 
                    if ( has_post_thumbnail(  $post->ID ) ) {
            ?>


            <div class="col ani<?php echo $i; ?> animated">
                <figure class=" p-2 m-1  m-2">
                    <a class="news-click" href="<?php echo the_permalink();?>"> </a>
                    <div class="news-thumb" style="height:200px;background:url(<?php echo get_the_post_thumbnail_url( $post->ID, 'large'); ?>) no-repeat; background-size:cover;background-position: center;"></div>
                    <figcaption>
                        <?php the_title( '<h2 class="entry-title content text-1 px-2 mb-2 pt-2 ">', '</h2>' ); ?>
                        <p class="text text-justify px-2 p-2 mb-0"><?php echo substr(strip_tags($post->post_content), 0, 150);?> </p>
                        
                         <p class="p-0"><a class="btn mt-0 alignright btn-xemthem" href="<?php echo the_permalink();?>"  role="button"></a></p>
                    </figcaption>


                </figure>
            </div>

            <?php    
                    }
                 $i++;
                    } //end while
                } 
            

            // Restore original Post Data
            wp_reset_postdata();
        ?>

        </div>
    </div>
    <!--<div class="wave-bottom ani2 animated"></div>-->
    <svg class="ani2 animated" viewBox="0 0 120 18" id="wavebot">

        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" stdDeviation="1" result="blur" />
                <feColorMatrix in="blur" mode="matrix" values="
           1 0 0 0 0  
           0 1 0 0 0  
           0 0 1 0 0  
           0 0 0 13 -9" result="goo" />
                <xfeBlend in="SourceGraphic" in2="goo" />

            </filter>
            <linearGradient id="grad2" x1="0%" y1="0%" x2="0%" y2="100%">
                <stop offset="0%" style="stop-color:#4CBBF2;stop-opacity:1" />
                <stop offset="50%" style="stop-color:#07629B;stop-opacity:1" />
                <stop offset="100%" style="stop-color:#004772;stop-opacity:1" />
            </linearGradient>
            <path id="wave" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" />
        </defs>
        <use id="wave1" class="wave" xlink:href="#wave" fill="url(#grad2)" x="0" y="-4"></use>
        <use id="wave3" class="wave" xlink:href="#wave" fill="url(#grad2)" x="0" y="-2"></use>
        <use id="wave2" class="wave" xlink:href="#wave" fill="url(#grad2)" x="0" y="0"></use>
        <use id="wave4" class="wave" xlink:href="#wave" fill="url(#grad2)" x="0" y="2"></use>

    </svg>
</div>
<?php if( have_rows('Partners') ):  $i = 1; ?>
<div class="home-section sectam partners" style="background:url(<?php echo get_stylesheet_directory_uri(); ?>/images/bg-partners.jpg) no-repeat center bottom; background-size:cover;">
    <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand custom-logo-link" rel="home">
            <img width="180" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-gold.png" class="img-fluid" alt="Asiana Luxury"></a>
    </div>
    <div class="container">
        <div class="row mb-2">

            <?php while( have_rows('Partners') ): the_row(); 

		// vars
		$image = get_sub_field('logo_partner');
		$title = get_sub_field('title_partner');
		$link = get_sub_field('link_partner');
        $numrows = count( get_sub_field( 'logo_partner' ) );
         if( $i==1 ) { 
		?>
            <div class="col-md-12 m-0 pb-5 text-center  ani<?php echo $i; ?> animated">
                <h3 class="text-1 text-uppercase text-white mb-4"><?php echo $title; ?></h3>
                <a href="<?php echo $link; ?>"><img class="cdt" src="<?php echo $image['url']; ?>" /></a>
            </div>


            <?php } else { ?>

            <div class="col m-0 pt-5 text-center ani<?php echo $i; ?> animated">
                <h5 class="text-1 text-uppercase text-white mb-3"><?php echo $title; ?></h5>
                <a href="<?php echo $link; ?>"><img class="partner" src="<?php echo $image['url']; ?>" /></a>
            </div>



            <?php } ?>
            <?php $i++; endwhile; ?>
        </div>
    </div>
    <div class="wave-bottom dark ani3 animated"></div>
</div>
<?php endif; ?>

<?php if( have_rows('lienhe') ):  ?>
<div class="home-section secchin lienhe" style="background:url(<?php echo get_stylesheet_directory_uri(); ?>/images/beachbg.jpg) no-repeat center bottom; background-size:cover;">
    <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand custom-logo-link" rel="home">
            <img width="180" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-gold.png" class="img-fluid" alt="Asiana Luxury"></a>
    </div>
    <div class="container">
        <div class="row">

            <div class="col col py-3 px-lg-5 ani1 animated">
               <img class="imgtext ani1 animated  headlienhe" src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/nhanthongtin.svg">
                <?php echo do_shortcode('[contact-form-7 id="134"]');?>
            </div>
            <div class="col text-lux col py-3 px-lg-5 ani2 animated">
                <?php while( have_rows('lienhe') ): the_row(); 
		// vars
		$tenvp = get_sub_field('ten_vp');
		$diachi = get_sub_field('diachi');
		$email = get_sub_field('email_vp');
		$dt = get_sub_field('dt_vp');
        if ($tenvp):
		?>

                <h3 class="heading text-uppercase  mb-2 mt-2 "><?php echo $tenvp; ?></h3>
                <p class="p-0 m-0"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $diachi; ?></p>
                <?php if ($dt):  ?><p><i class="fa fa-envelope"></i> <?php echo $dt; ?></p> <?php endif; ?>
                <?php if ($email):  ?><p><i class="fa fa-phone-square"></i> <?php echo $email; ?></p><?php endif; ?>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
      <!--  <div class="wave-bottom dark ani3 animated"></div>-->
    </div>
    </div>
    <?php endif; ?>
    <?php

get_footer();
