<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

</div><!-- fullpage end -->
 

<div class="wrapper p-1  " id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?> d-none d-sm-none d-md-block">

		<div class="row">

		
            <div class="col align-self-end right-footer fixed-bottom">
                
                <div class="d-flex  justify-content-end list-inline">
                    <p class="list-inline-item mb-0  text-lux text-right text-uppercase">Copyright © 2020 by Gotec Land. All Rights Reserved.</p>
                    <a href="https://www.facebook.com/gotecland.vn/" target="_blank" class="list-inline-item btn text-right btn-social btn-lg btn-facebook">
                        <span class="fa fa-facebook text-lux"></span> 
                      </a> 
                    <a href="https://www.youtube.com/channel/UChV4GDMJbKNzUvi4bUDme1w/" target="_blank"  class="list-inline-item btn text-right text-lux btn-lg btn-social btn-youtube">
                        <span class="fa fa-youtube text-lux"></span> 
                      </a>

                </div>
                   
            </div><!--col end -->
		</div><!-- row end -->

	</div><!-- container end -->
<div class="lienhe overlay overlay-hugeinc">
			<button type="button" class="overlay-close">Close</button>
    <div class="fullscreenform">
            <h3 class=" text-lux text-center mb-4 mt-2"> ĐĂNG KÝ NHẬN THÔNG TIN</h3>
			 <?php echo do_shortcode('[contact-form-7 id="134"]');?>
        </div>
		</div>
</div><!-- wrapper end -->
<div id="go-top"></div>
</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</script>

</body>

</html>

