<?php
/*
Template Name: Homepage
*/
get_header();
?>


<div class="home-section secmot intro">
    <div class="container-fluid">
        <div class="row">

            <!--Carousel Wrapper-->
            <div id="hometopslider" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
               

                <!--Slides-->
                <div class="carousel-inner" role="listbox">
                    <?php if( have_rows('slider') ):  $i = 0; ?>
                    <?php while( have_rows('slider') ): the_row(); 
                        // vars
                        $image = get_sub_field('slide_image');
                      
   
                        $imagetext = get_sub_field('slide_text_image');
                        $head = get_sub_field('heading_slide');
                        $content = get_sub_field('content_slide');
                        $link = get_sub_field('link_slide');
            
                        if ($i == 0) { $ac = 'active'; } else { $ac = ''; };
                        ?>
                    <!--First slide-->
                    <div class="carousel-item <?php echo $ac; ?>">
                        <!--Mask-->
                        <div class="view">
                            <!--Video source-->
                            <?php    $linkvideo = get_sub_field('slide_video'); 
                    if (empty($linkvideo)) :
                    $linkvideo = get_stylesheet_directory_uri() . '/video/intro.mp4';
                    endif; 
                    ?>
                            <video id="video_intro" data-autoplay loop playsinline data-keepplaying muted>
                                <source src="<?php echo esc_url($linkvideo); ?>" type="video/mp4" />
                            </video>
                            <?php // endif; ?>
                            
                        </div>
                        <!--/Mask-->
                    </div>
                    <!--/First slide-->

                    <!--/Third slide-->

                    <?php $i++; endwhile; ?>
                    <?php endif; //image ?>
                </div>
                <!--/.Slides-->

                <!--Controls-->
              <!--  <a class="carousel-control-prev" href="#hometopslider" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#hometopslider" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>-->
                <!--/.Controls-->
            </div>
            <!--/.Carousel Wrapper-->

        </div>
    </div>
    <script type="text/javascript">
        (function($) {
            jQuery(document).ready(function($) {
                var video = document.getElementById("video_intro");
                video.addEventListener("canplay", function() {
                    setTimeout(function() {
                        video.play();
                    }, 5000);
                });
            });
        })();

    </script>

</div>
<?php
        if ( ! have_rows( 'gioithieu' ) ) {
        return false;
        }

        if ( have_rows( 'gioithieu' ) ) : ?>

<?php while ( have_rows( 'gioithieu' ) ) : the_row(); 
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $content = get_sub_field( 'content' );
        $link = get_sub_field('link');
        $imgtext = get_sub_field( 'image_text_title');
        ?>
<div class="home-section sechai gioithieu" style="background-image:url('<?php echo $bg['url']; ?>');">
    <div class="bongtop"></div>
    <div class="container-fluid">
        <div class="row  justify-content-center">

            <div class="col-md-6 col-lg-6  col-xl-6 text-center  ">
                <div class="jumbotron  bg-transparent p-0">
                   <!-- <div class="bong"></div>-->
                    <?php if( !empty( $imgtext ) ): ?>
                    <img class="imgtext mb-3 text-center w-50" src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $head; ?>">
                    <?php else:  ?>
                    <h2 class="display-4 text-lux text-left line-wave mb-4"> <?php echo $head; ?></h2>
                    <?php endif; ?>


                    <p class="content text-white text-normal text-center"> <?php echo $content; ?></p>
                    <?php 
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                
                ?>
                    <a class="btn  btn-simple p-0  text-center" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button"><span><?php echo esc_html($link_title); ?></span></a>



                    <?php endif; ?>
                </div>
                <!--col -->
            </div>
            <!--jumbotron -->
        </div>

    </div>
     <div class="bongbot"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>


<?php //Sec vitri
        if ( ! have_rows( 'vitri' ) ) {
        return false;
        }

        if ( have_rows( 'vitri' ) ) : ?>

<?php while ( have_rows( 'vitri' ) ) : the_row(); 
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $imgtext = get_sub_field( 'Image_text_title');
        $content = get_sub_field( 'content' );
        $link = get_sub_field('link');

        ?>
<!--<div class="home-section secba vitri" style="background:url('<?php echo $bg['url']; ?>')  -200px center/ auto 85vh no-repeat rgb(10, 78, 115)">-->
<div class="home-section secba vitri" >
 <div class="bongtop"></div>
    <div class="container-fluid">
        <div class="row">

            <div class="d-md-flex align-items-center">

               
                <div class="col-md-4 col-lg-4 d-md-flex ">

                    <div class="box-vitri  bg-transparent align-self-center">
                         <div class="bong"></div>
                        <?php if( !empty( $imgtext ) ): ?>
                        <img class="imgtext mb-3" src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $head; ?>">
                        <?php else:  ?>
                        <h2 class="display-4 text-lux text-left line-wave mb-4"> <?php echo $head; ?></h2>
                        <?php endif; ?>


                        <p class="content text-white text-normal text-justify "> <?php echo $content; ?></p>
                        <?php 
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                
                ?>
                        <a class="btn  btn-simple p-0 align-right text-center" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" role="button"><?php echo esc_html($link_title); ?></a>

                        <?php endif; ?>
                    </div>
                    <!--col -->
                </div>
                <!--jumbotron -->
                 <div id="mapimg" class="col-md-8 col-lg-8 p-0">
                    <img class="mapnew alignright" src="<?php echo $bg['url']; ?>" alt="<?php echo $head; ?>">
                    <!-- <img class="line" src="<?php echo get_stylesheet_directory_uri(); ?>/images/map/line-diamond-01.svg" alt="Vị trí độc tôn">-->
                     <a class="pointer-map" href="./vi-tri/">
                    <h3>xem chi tiết</h3>
                    <div class="pointer">
                        <span></span><span></span><span></span>
                       <svg version="1.1" id="icon-pointer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1200 889.1" style="enable-background:new 0 0 1200 889.1;" xml:space="preserve">
<style type="text/css">
	.st0{fill:url(#SVGID_1_);}
	.st1{fill:url(#SVGID_2_);}
	.st2{fill:url(#SVGID_3_);}
	.st3{fill:url(#SVGID_4_);}
	.st4{fill:url(#SVGID_5_);}
	.st5{fill:#0C6D3B;}
	.st6{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:0.1518;stroke-miterlimit:10;}
	.st7{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:5.669546e-02;stroke-miterlimit:10;}
	.st8{fill:#A47752;}
	.st9{fill:url(#SVGID_6_);}
	.st10{fill:url(#SVGID_7_);}
	.st11{fill:url(#SVGID_8_);}
	.st12{fill:url(#SVGID_9_);}
	.st13{fill:url(#SVGID_10_);}
	.st14{fill:url(#SVGID_11_);}
	.st15{fill:url(#SVGID_12_);}
	.st16{fill:url(#SVGID_13_);}
	.st17{fill:url(#SVGID_14_);}
	.st18{fill:url(#SVGID_15_);}
	.st19{fill:url(#SVGID_16_);}
	.st20{fill:url(#SVGID_17_);}
	.st21{fill:url(#SVGID_18_);}
	.st22{fill:url(#SVGID_19_);}
	.st23{fill:url(#SVGID_20_);}
	.st24{fill:url(#SVGID_21_);}
	.st25{fill:url(#SVGID_22_);}
	.st26{fill:url(#SVGID_23_);}
	.st27{fill:url(#SVGID_24_);}
	.st28{fill:url(#SVGID_25_);}
	.st29{fill:url(#SVGID_26_);}
	.st30{fill:url(#SVGID_27_);}
	.st31{fill:url(#SVGID_28_);}
	.st32{fill:url(#SVGID_29_);}
	.st33{fill:url(#SVGID_30_);}
	.st34{fill:url(#SVGID_31_);}
	.st35{fill:url(#SVGID_32_);}
</style>
<g>
	<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="674.925" y1="302.5417" x2="686.8749" y2="302.5417">
		<stop  offset="0.1816" style="stop-color:#D3AC89"/>
		<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
	</linearGradient>
	<circle class="st0" cx="680.9" cy="302.5" r="6"/>
	<linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="675.9764" y1="302.5417" x2="685.8235" y2="302.5417">
		<stop  offset="0.1816" style="stop-color:#D3AC89"/>
		<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
	</linearGradient>
	<circle class="st1" cx="680.9" cy="302.5" r="4.9"/>
	<linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="677.0797" y1="302.5417" x2="684.7203" y2="302.5417">
		<stop  offset="0.1816" style="stop-color:#D3AC89"/>
		<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
	</linearGradient>
	<circle class="st2" cx="680.9" cy="302.5" r="3.8"/>
</g>
<g>
	<g>
		<g>
			<g>
				<g>
					
						<radialGradient id="SVGID_4_" cx="729.2495" cy="639.1733" r="45.7982" gradientTransform="matrix(1.015 0 0 -1.0169 -51.7814 906.8054)" gradientUnits="userSpaceOnUse">
						<stop  offset="0" style="stop-color:#F4F5F5"/>
						<stop  offset="0.3377" style="stop-color:#F1F2F2"/>
						<stop  offset="0.5396" style="stop-color:#E9EAEB"/>
						<stop  offset="0.7056" style="stop-color:#DBDEDF"/>
						<stop  offset="0.8522" style="stop-color:#C8CCCF"/>
						<stop  offset="0.9847" style="stop-color:#B2B8BC"/>
						<stop  offset="1" style="stop-color:#AFB6BA"/>
					</radialGradient>
					<path class="st3" d="M680.9,303.3c-1.6,0-2.4-2.1-4.2-7.2c-1.1-3.1-2.7-7.3-5-12.6c-1.9-4.4-4.5-8.5-7-12.5
						c-4.2-6.6-8.1-12.9-8.9-20.2c-0.8-7,1.4-13.7,6-18.9c4.8-5.4,11.7-8.4,19.1-8.4c7.3,0,14.3,3.1,19.1,8.4
						c4.6,5.2,6.8,11.9,6,18.9c-0.8,7.3-4.7,13.6-8.9,20.2c-2.5,4-5,8-7,12.5c-2.3,5.3-3.9,9.5-5,12.6
						C683.3,301.2,682.5,303.3,680.9,303.3z"/>
					
						<linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="656.3532" y1="628.6" x2="705.4467" y2="628.6" gradientTransform="matrix(1 0 0 -1 0 892)">
						<stop  offset="0" style="stop-color:#F4F5F5"/>
						<stop  offset="9.562187e-02" style="stop-color:#EFF0F0"/>
						<stop  offset="0.2027" style="stop-color:#E2E4E5"/>
						<stop  offset="0.3154" style="stop-color:#CDD0D2"/>
						<stop  offset="0.4312" style="stop-color:#B1B8BC"/>
						<stop  offset="0.4413" style="stop-color:#AFB6BA"/>
					</linearGradient>
					<path class="st4" d="M680.9,224.2c-14.3,0-26,11.9-24.4,26.5c1.3,11.5,10.7,20.7,15.8,32.5c5.6,12.9,6.7,19.4,8.6,19.4
						c1.9,0,2.9-6.5,8.6-19.4c5.1-11.8,14.5-20.9,15.8-32.5C706.9,236.1,695.2,224.2,680.9,224.2z"/>
				</g>
				<g>
					<path class="st5" d="M680.9,300.5c-1.5,0-2.2-2-4-6.8c-1.1-2.9-2.5-6.9-4.7-11.8c-1.8-4.1-4.2-8-6.6-11.7
						c-3.9-6.2-7.6-12.1-8.4-19c-0.7-6.6,1.3-12.8,5.7-17.7c4.5-5,11.1-7.9,18-7.9s13.5,2.9,18.1,7.9c4.4,4.9,6.4,11.1,5.7,17.7
						c-0.8,6.9-4.5,12.7-8.4,19c-2.3,3.7-4.8,7.5-6.6,11.7c-2.2,4.9-3.6,8.9-4.7,11.8C683.1,298.5,682.4,300.5,680.9,300.5z"/>
				</g>
			</g>
		</g>
		<g>
			<g>
				<g>
					<g>
						<path class="st6" d="M665.5,251.9c0.2,0,0.4,0,0.6,0c0.2,0,0.4,0,0.6,0.1c0.2,0,0.5,0.1,0.7,0.2c0.2,0.1,0.4,0.3,0.6,0.5
							c0.2,0.2,0.3,0.4,0.4,0.7c0.1,0.3,0.1,0.7,0.1,1.1s-0.1,0.8-0.2,1.2c-0.1,0.3-0.3,0.6-0.6,0.8c-0.5,0.4-1,0.5-1.7,0.5
							c-0.2,0-0.5,0-0.7,0h-0.4v-5h0.6V251.9z M665.5,256.9c0.1,0,0.2,0,0.4,0s0.3,0,0.3,0c0.1,0,0.2,0,0.3-0.1
							c0.1,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.2,0.4-0.3c0.1-0.1,0.2-0.3,0.3-0.4c0.2-0.4,0.3-0.9,0.3-1.5c0-0.5-0.1-0.8-0.2-1.2
							c-0.1-0.3-0.2-0.6-0.4-0.7c-0.1-0.2-0.3-0.3-0.5-0.4c-0.2-0.1-0.4-0.1-0.5-0.2c-0.1,0-0.3,0-0.4,0c-0.2,0-0.3,0-0.5,0v4.9
							H665.5z"/>
						<path class="st6" d="M669.9,252L669.9,252l1.3-0.1v0.1h-0.4v4.9h0.4v0.1h-1.3v-0.1h0.3V252H669.9z"/>
						<path class="st6" d="M672.7,257L672.7,257l2.1-5h0.1l1.8,5.1H676l-0.6-1.6h-2.1L672.7,257z M674.5,252.7l-1.1,2.6h2
							L674.5,252.7z"/>
						<path class="st6" d="M678.1,257L678.1,257l0.5-5h0.2l1.5,4.7l1.5-4.7h0.2l0.6,5H682l-0.5-4l-1.3,4h-0.5l-1.3-4L678.1,257z"/>
						<path class="st6" d="M684.2,254.6c0-0.9,0.2-1.5,0.6-2c0.4-0.5,1-0.7,1.6-0.7c0.7,0,1.2,0.2,1.6,0.6c0.4,0.4,0.6,1,0.6,1.9
							s-0.2,1.5-0.6,2c-0.4,0.4-1,0.7-1.7,0.7c-0.6,0-1.1-0.2-1.5-0.5c-0.2-0.2-0.4-0.4-0.5-0.8C684.2,255.4,684.2,255,684.2,254.6z
							 M686.4,252c-0.5,0-0.9,0.2-1.2,0.6c-0.3,0.4-0.5,1.1-0.5,1.9c0,0.8,0.1,1.4,0.4,1.8c0.3,0.4,0.7,0.6,1.2,0.6s0.9-0.2,1.2-0.7
							c0.3-0.4,0.5-1.1,0.5-1.9c0-0.8-0.1-1.4-0.4-1.8C687.3,252.2,686.9,252,686.4,252z"/>
						<path class="st6" d="M690.2,257L690.2,257l-0.1-5h0.2l3,4.7V252h0.1v5h-0.5l-2.7-4.2V257z"/>
						<path class="st6" d="M695.7,251.9c0.2,0,0.4,0,0.6,0c0.2,0,0.4,0,0.6,0.1c0.2,0,0.5,0.1,0.7,0.2c0.2,0.1,0.4,0.3,0.6,0.5
							c0.2,0.2,0.3,0.4,0.4,0.7c0.1,0.3,0.1,0.7,0.1,1.1s-0.1,0.8-0.2,1.2c-0.1,0.3-0.3,0.6-0.6,0.8c-0.5,0.4-1,0.5-1.7,0.5
							c-0.2,0-0.5,0-0.7,0h-0.4v-5h0.6V251.9z M695.7,256.9c0.1,0,0.2,0,0.4,0s0.3,0,0.3,0c0.1,0,0.2,0,0.3-0.1
							c0.1,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.2,0.4-0.3c0.1-0.1,0.2-0.3,0.3-0.4c0.2-0.4,0.3-0.9,0.3-1.5c0-0.5-0.1-0.8-0.2-1.2
							c-0.1-0.3-0.2-0.6-0.4-0.7c-0.1-0.2-0.3-0.3-0.5-0.4c-0.2-0.1-0.4-0.1-0.5-0.2c-0.1,0-0.3,0-0.4,0c-0.2,0-0.3,0-0.5,0v4.9
							H695.7z"/>
					</g>
					<g>
						<g>
							<path class="st7" d="M675.2,261.1c-0.4,0-0.6-0.1-0.8-0.3c-0.1-0.1-0.1-0.2-0.1-0.3c0-0.1-0.1-0.3-0.1-0.4s0-0.3,0.1-0.5
								c0.1-0.1,0.1-0.2,0.2-0.3c0.2-0.1,0.4-0.2,0.7-0.2c0.1,0,0.1,0,0.2,0s0.1,0,0.2,0h0.1l0,0c-0.1-0.1-0.3-0.1-0.4-0.1
								c-0.2,0-0.4,0.1-0.5,0.2s-0.2,0.2-0.2,0.4c0,0.1,0,0.2,0,0.4c0,0.2,0,0.3,0.1,0.4c0,0.1,0.1,0.2,0.1,0.3
								c0.1,0.1,0.1,0.1,0.2,0.1c0.1,0,0.1,0.1,0.2,0.1C675.1,261,675.1,261,675.2,261.1c0.1,0,0.2,0,0.3,0s0.1,0,0.2,0h0.1l0,0
								C675.5,261,675.4,261.1,675.2,261.1z"/>
							<path class="st7" d="M677.8,259.1L677.8,259.1h-0.9v0.9h0.8l0,0h-0.8v1.1h1l0,0h-1.2v-2H677.8z"/>
							<path class="st7" d="M678.9,261.1L678.9,261.1v-2h0.1l1.2,1.8v-1.8l0,0v2H680l-1.1-1.7V261.1z"/>
							<path class="st7" d="M681.9,261.1h-0.2v-2h-0.5l0,0h1.2l0,0h-0.5V261.1z"/>
							<path class="st7" d="M683.4,259.1c0.2,0,0.4,0,0.6,0c0.5,0,0.7,0.2,0.7,0.5c0,0.1,0,0.3-0.1,0.4s-0.2,0.2-0.3,0.2l0.6,1h-0.3
								l-0.6-0.9h-0.4v0.9h-0.2V259.1z M684,259.1c-0.1,0-0.2,0-0.3,0v1h0.4c0.3,0,0.4-0.2,0.4-0.5c0-0.1,0-0.3-0.1-0.3
								C684.3,259.1,684.2,259.1,684,259.1z"/>
							<path class="st7" d="M685.8,261.1L685.8,261.1l0.8-2l0,0l0.7,2h-0.2l-0.2-0.6H686L685.8,261.1z M686.5,259.4l-0.4,1h0.8
								L686.5,259.4z"/>
							<path class="st7" d="M689.4,261.1h-1.1v-2h0.2v2H689.4L689.4,261.1z"/>
						</g>
					</g>
					<rect x="662.7" y="259.8" class="st8" width="9.6" height="0.3"/>
					<rect x="690.7" y="259.8" class="st8" width="9.6" height="0.3"/>
				</g>
				<g>
					<g>
						
							<linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="681.8159" y1="658.05" x2="683.5494" y2="658.05" gradientTransform="matrix(1 0 0 -1 0 892)">
							<stop  offset="0.1816" style="stop-color:#D3AC89"/>
							<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
						</linearGradient>
						<polygon class="st9" points="681.8,229.5 683.5,232 681.8,238.4 						"/>
						<polygon class="st8" points="681.8,229.5 680.1,232 681.8,238.4 						"/>
					</g>
					<g>
						
							<linearGradient id="SVGID_7_" gradientUnits="userSpaceOnUse" x1="2142.1987" y1="1674.9749" x2="2143.9399" y2="1674.9749" gradientTransform="matrix(0.5 -0.866 -0.866 -0.5 1056.0645 2929.6064)">
							<stop  offset="0.1816" style="stop-color:#D3AC89"/>
							<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
						</linearGradient>
						<polygon class="st10" points="672.8,234.7 675.8,234.4 680.5,239.2 						"/>
						<polygon class="st8" points="672.8,234.7 674.1,237.5 680.5,239.2 						"/>
					</g>
					<g>
						
							<linearGradient id="SVGID_8_" gradientUnits="userSpaceOnUse" x1="3753.0459" y1="918.6052" x2="3754.7839" y2="918.6052" gradientTransform="matrix(-0.5 -0.866 -0.866 0.5 3348.7302 3033.8132)">
							<stop  offset="0.1816" style="stop-color:#D3AC89"/>
							<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
						</linearGradient>
						<polygon class="st11" points="672.8,245.1 674.1,242.4 680.5,240.7 						"/>
						<polygon class="st8" points="672.8,245.1 675.8,245.4 680.5,240.7 						"/>
					</g>
					<g>
						
							<linearGradient id="SVGID_9_" gradientUnits="userSpaceOnUse" x1="3903.5159" y1="-854.5137" x2="3905.2495" y2="-854.5137" gradientTransform="matrix(-1 0 0 1 4585.332 1100.4137)">
							<stop  offset="0.1816" style="stop-color:#D3AC89"/>
							<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
						</linearGradient>
						<polygon class="st12" points="681.8,250.4 680.1,247.9 681.8,241.4 						"/>
						<polygon class="st8" points="681.8,250.4 683.5,247.9 681.8,241.4 						"/>
					</g>
					<g>
						
							<linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="2443.1355" y1="-1871.4828" x2="2444.8767" y2="-1871.4828" gradientTransform="matrix(-0.5 0.866 0.866 0.5 3529.2673 -937.1928)">
							<stop  offset="0.1816" style="stop-color:#D3AC89"/>
							<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
						</linearGradient>
						<polygon class="st13" points="690.8,245.1 687.8,245.4 683.1,240.7 						"/>
						<polygon class="st8" points="690.8,245.1 689.5,242.4 683.1,240.7 						"/>
					</g>
					<g>
						
							<linearGradient id="SVGID_11_" gradientUnits="userSpaceOnUse" x1="832.2693" y1="-1115.222" x2="834.0073" y2="-1115.222" gradientTransform="matrix(0.5 0.866 0.866 -0.5 1236.6014 -1041.3997)">
							<stop  offset="0.1816" style="stop-color:#D3AC89"/>
							<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
						</linearGradient>
						<polygon class="st14" points="690.8,234.7 689.5,237.5 683.1,239.2 						"/>
						<polygon class="st8" points="690.8,234.7 687.8,234.4 683.1,239.2 						"/>
					</g>
					<g>
						<g>
							
								<linearGradient id="SVGID_12_" gradientUnits="userSpaceOnUse" x1="440.2898" y1="382.7315" x2="442.7868" y2="382.7315" gradientTransform="matrix(0.4498 0.8931 0.8931 -0.4498 145.0135 11.4349)">
								<stop  offset="0.1816" style="stop-color:#D3AC89"/>
								<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
							</linearGradient>
							<polygon class="st15" points="686.6,231.7 686.3,234.6 684.3,235.6 							"/>
						</g>
						<g>
							<polygon class="st8" points="686.6,231.7 684.2,233.4 684.3,235.6 							"/>
						</g>
					</g>
					<g>
						<g>
							
								<linearGradient id="SVGID_13_" gradientUnits="userSpaceOnUse" x1="2091.6321" y1="-280.6396" x2="2094.1228" y2="-280.6396" gradientTransform="matrix(-0.5485 0.8361 0.8361 0.5485 2071.6597 -1356.0601)">
								<stop  offset="0.1816" style="stop-color:#D3AC89"/>
								<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
							</linearGradient>
							<polygon class="st16" points="691.3,239.9 688.6,241.1 686.8,239.9 							"/>
						</g>
						<g>
							<polygon class="st8" points="691.3,239.9 688.6,238.7 686.8,239.9 							"/>
						</g>
					</g>
					<g>
						<g>
							
								<linearGradient id="SVGID_14_" gradientUnits="userSpaceOnUse" x1="3491.6899" y1="817.9297" x2="3494.1809" y2="817.9297" gradientTransform="matrix(-0.9984 -5.698544e-02 -5.698544e-02 0.9984 4219.314 -371.3799)">
								<stop  offset="0.1816" style="stop-color:#D3AC89"/>
								<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
							</linearGradient>
							<polygon class="st17" points="686.6,248.2 684.2,246.4 684.3,244.2 							"/>
						</g>
						<g>
							<polygon class="st8" points="686.6,248.2 686.3,245.2 684.3,244.2 							"/>
						</g>
					</g>
					<g>
						<g>
							
								<linearGradient id="SVGID_15_" gradientUnits="userSpaceOnUse" x1="3240.4128" y1="2579.7793" x2="3242.9099" y2="2579.7793" gradientTransform="matrix(-0.4498 -0.8931 -0.8931 0.4498 4440.3184 1980.9789)">
								<stop  offset="0.1816" style="stop-color:#D3AC89"/>
								<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
							</linearGradient>
							<polygon class="st18" points="677.1,248.2 677.4,245.2 679.3,244.2 							"/>
						</g>
						<g>
							<polygon class="st8" points="677.1,248.2 679.4,246.4 679.3,244.2 							"/>
						</g>
					</g>
					<g>
						<g>
							
								<linearGradient id="SVGID_16_" gradientUnits="userSpaceOnUse" x1="1589.0978" y1="3243.0171" x2="1591.5884" y2="3243.0171" gradientTransform="matrix(0.5485 -0.8361 -0.8361 -0.5485 2513.6721 3348.4739)">
								<stop  offset="0.1816" style="stop-color:#D3AC89"/>
								<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
							</linearGradient>
							<polygon class="st19" points="672.3,239.9 675,238.7 676.8,239.9 							"/>
						</g>
						<g>
							<polygon class="st8" points="672.3,239.9 675,241.1 676.8,239.9 							"/>
						</g>
					</g>
					<g>
						<g>
							
								<linearGradient id="SVGID_17_" gradientUnits="userSpaceOnUse" x1="189.0382" y1="2144.4182" x2="191.5292" y2="2144.4182" gradientTransform="matrix(0.9984 5.698544e-02 5.698544e-02 -0.9984 366.0179 2363.7937)">
								<stop  offset="0.1816" style="stop-color:#D3AC89"/>
								<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
							</linearGradient>
							<polygon class="st20" points="677.1,231.7 679.4,233.4 679.3,235.6 							"/>
						</g>
						<g>
							<polygon class="st8" points="677.1,231.7 677.4,234.6 679.3,235.6 							"/>
						</g>
					</g>
				</g>
			</g>
		</g>
	</g>
	<g>
		
			<linearGradient id="SVGID_18_" gradientUnits="userSpaceOnUse" x1="627.2" y1="571.7" x2="729.126" y2="571.7" gradientTransform="matrix(1 0 0 -1 0 892)">
			<stop  offset="0.1816" style="stop-color:#D3AC89"/>
			<stop  offset="0.8128" style="stop-color:#D5AF8D"/>
		</linearGradient>
		<path class="st21" d="M728.7,328.3H627.6c-0.2,0-0.4-0.2-0.4-0.4v-15.2c0-0.2,0.2-0.4,0.4-0.4h101.1c0.2,0,0.4,0.2,0.4,0.4v15.2
			C729.2,328.1,729,328.3,728.7,328.3z"/>
		<g>
			
				<linearGradient id="SVGID_19_" gradientUnits="userSpaceOnUse" x1="631.2728" y1="571.55" x2="638.3148" y2="571.55" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st22" d="M638.3,320.1c0,1.2-0.4,2.2-1.3,2.9c-0.4,0.3-0.8,0.5-1.2,0.6s-1,0.2-1.7,0.2c-0.2,0-0.6,0-1.3,0
				c-0.3,0-0.6,0-0.8,0h-0.3c0-0.1,0-0.3,0.1-0.4c0-0.4,0.1-0.9,0.1-1.4v-3.1c0-0.5,0-0.8-0.1-1c0-0.2-0.2-0.3-0.5-0.3h-0.1l0.2-0.5
				h0.3h0.9c0.2,0,0.6,0,0.9,0c0.4,0,0.6,0,0.7,0c0.4,0,0.7,0,1,0c0.8,0,1.4,0.2,1.8,0.4C637.9,318.1,638.3,318.9,638.3,320.1z
				 M636.6,320.5c0-0.9-0.2-1.5-0.6-2c-0.2-0.2-0.4-0.4-0.7-0.5s-0.6-0.1-1.1-0.1c-0.3,0-0.5,0-0.8,0v1v2.9c0,0.1,0,0.3,0,0.4
				c0,0.3,0.1,0.5,0.2,0.6c0.1,0.1,0.4,0.2,0.9,0.2c0.7,0,1.2-0.2,1.6-0.7C636.4,322,636.6,321.3,636.6,320.5z"/>
			
				<linearGradient id="SVGID_20_" gradientUnits="userSpaceOnUse" x1="638.7999" y1="571.55" x2="641.1062" y2="571.55" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st23" d="M641.1,323.8h-0.4h-1h-0.4c0-0.1,0-0.3,0.1-0.4c0-0.3,0.1-0.8,0.1-1.4v-3.1c0-0.4,0-0.8-0.1-1
				c0-0.2-0.2-0.3-0.5-0.3h-0.1l0.2-0.5h0.4h1.1h0.5c-0.1,0.6-0.1,1.2-0.1,1.6v3.2C640.9,322.8,641,323.4,641.1,323.8z"/>
			
				<linearGradient id="SVGID_21_" gradientUnits="userSpaceOnUse" x1="641.5355" y1="571.7" x2="648.349" y2="571.7" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st24" d="M648.3,323.8h-0.4h-1h-0.6c0-0.4-0.1-0.9-0.3-1.4l-0.3-0.8H645h-1h-0.7l-0.3,0.8c-0.1,0.4-0.2,0.7-0.2,0.9
				c0,0.1,0,0.3,0.1,0.5h-0.3H642h-0.4v-0.1c0.2-0.3,0.3-0.6,0.5-1c0.1-0.2,0.1-0.4,0.3-0.6l1.5-3.9c0.2-0.4,0.3-0.8,0.4-1.2h0.4
				l0.8-0.1l0.4-0.1c0.1,0.3,0.1,0.6,0.3,1l1.5,4.2c0.1,0.3,0.3,0.8,0.5,1.3C648.2,323.6,648.3,323.7,648.3,323.8z M645.6,320.9
				l-0.9-2.8l-1,2.8h0.6h0.7H645.6z"/>
			
				<linearGradient id="SVGID_22_" gradientUnits="userSpaceOnUse" x1="648.683" y1="571.45" x2="657.3262" y2="571.45" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st25" d="M657.3,323.8h-0.4h-1h-0.4c0-0.3,0-0.5,0-0.8c0-0.4,0-0.7,0-0.9l-0.2-3.5l-1.8,4.1
				c-0.1,0.2-0.1,0.3-0.2,0.6c-0.1,0.2-0.1,0.4-0.2,0.6h-0.3h-0.4h-0.3c0-0.1,0-0.2,0-0.2c0-0.2-0.1-0.5-0.3-0.9l-1.7-4.1v0.8
				l-0.2,2.4c0,0.3,0,0.5,0,0.8c0,0.5,0,0.9,0.1,1.1h-0.4h-0.5h-0.4v-0.1c0-0.1,0.1-0.2,0.1-0.3c0.1-0.4,0.1-0.9,0.2-1.5l0.2-2.9
				c0-0.3,0-0.5,0-0.6v-0.1c0-0.3,0-0.4-0.1-0.5s-0.2-0.1-0.4-0.1h-0.1l0.2-0.5h0.5h1.3h0.6c0.1,0.4,0.2,0.8,0.4,1.1l1.5,3.7
				l0.3-0.6l1.3-3c0.1-0.3,0.2-0.7,0.4-1.1h0.5h1.5c0,0.3,0,0.4,0,0.6c0,0.3,0,0.7,0,1l0.2,3.3c0,0.5,0.1,1,0.2,1.4
				C657.2,323.6,657.3,323.7,657.3,323.8z"/>
			
				<linearGradient id="SVGID_23_" gradientUnits="userSpaceOnUse" x1="657.973" y1="571.45" x2="665.2534" y2="571.45" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st26" d="M665.3,320.4c0,1.1-0.4,2-1.1,2.7c-0.7,0.7-1.6,1-2.7,1c-1,0-1.9-0.3-2.5-1c-0.6-0.6-1-1.5-1-2.5
				c0-1.1,0.4-1.9,1.1-2.6c0.7-0.7,1.6-1,2.7-1c0.7,0,1.3,0.1,1.8,0.4s1,0.7,1.3,1.2S665.3,319.7,665.3,320.4z M663.6,320.6
				c0-0.9-0.2-1.6-0.5-2.2c-0.3-0.5-0.8-0.8-1.4-0.8c-0.6,0-1.1,0.2-1.5,0.7c-0.3,0.5-0.5,1.1-0.5,2s0.2,1.6,0.5,2.2
				c0.4,0.5,0.8,0.8,1.5,0.8c0.6,0,1.1-0.2,1.4-0.7S663.6,321.5,663.6,320.6z"/>
			
				<linearGradient id="SVGID_24_" gradientUnits="userSpaceOnUse" x1="665.8" y1="571.45" x2="672.4327" y2="571.45" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st27" d="M672.4,323.9h-0.6h-0.6c-0.3-0.5-0.5-0.8-0.7-1l-3.1-4v0.5v2.6c0,0.5,0,1,0.1,1.4c0,0.1,0.1,0.3,0.1,0.4
				h-0.4h-0.5h-0.4c0-0.1,0-0.3,0.1-0.4c0-0.3,0.1-0.8,0.1-1.4v-3c0-0.5,0-0.8-0.1-1c0-0.2-0.2-0.3-0.5-0.3h-0.1l0.2-0.5h0.4h1.1
				h0.6c0.3,0.5,0.5,0.8,0.7,1.1l2.5,3.4v-2.5c0-0.6-0.1-1.1-0.1-1.5c0-0.2-0.1-0.3-0.1-0.4h0.4h0.5h0.4c0,0.1-0.1,0.3-0.1,0.4
				c0,0.3-0.1,0.8-0.1,1.6v2.9c0,0.1,0,0.2,0,0.6C672.3,323,672.4,323.5,672.4,323.9z"/>
			
				<linearGradient id="SVGID_25_" gradientUnits="userSpaceOnUse" x1="673.3055" y1="571.55" x2="680.3477" y2="571.55" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st28" d="M680.3,320.1c0,1.2-0.4,2.2-1.3,2.9c-0.4,0.3-0.8,0.5-1.2,0.6s-1,0.2-1.7,0.2c-0.2,0-0.6,0-1.3,0
				c-0.3,0-0.6,0-0.8,0h-0.3c0-0.1,0-0.3,0.1-0.4c0-0.4,0.1-0.9,0.1-1.4v-3.1c0-0.5,0-0.8-0.1-1c0-0.2-0.2-0.3-0.5-0.3h-0.1l0.2-0.5
				h0.3h0.9c0.2,0,0.6,0,0.9,0c0.4,0,0.6,0,0.7,0c0.4,0,0.7,0,1,0c0.8,0,1.4,0.2,1.8,0.4C679.9,318.1,680.3,318.9,680.3,320.1z
				 M678.7,320.5c0-0.9-0.2-1.5-0.6-2c-0.2-0.2-0.4-0.4-0.7-0.5s-0.6-0.1-1.1-0.1c-0.3,0-0.5,0-0.8,0v1v2.9c0,0.1,0,0.3,0,0.4
				c0,0.3,0.1,0.5,0.2,0.6c0.1,0.1,0.4,0.2,0.9,0.2c0.7,0,1.2-0.2,1.6-0.7C678.5,322,678.7,321.3,678.7,320.5z"/>
			
				<linearGradient id="SVGID_26_" gradientUnits="userSpaceOnUse" x1="683.2635" y1="571.65" x2="689.2007" y2="571.65" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st29" d="M689.2,317.3l-0.1,0.4l-0.1,0.6l-0.1,0.4h-0.2c-0.2-0.3-0.4-0.5-0.7-0.7s-0.7-0.3-1-0.3
				c-0.6,0-1.1,0.2-1.5,0.7s-0.5,1.1-0.5,1.9s0.2,1.5,0.6,2c0.4,0.5,1,0.8,1.7,0.8c0.3,0,0.6-0.1,0.9-0.2c0.3-0.1,0.5-0.2,0.7-0.4
				l0.2,0.1l-0.1,0.3l-0.1,0.4l-0.1,0.3c-0.4,0.1-0.7,0.2-0.9,0.2c-0.4,0.1-0.7,0.1-1.1,0.1c-0.7,0-1.2-0.1-1.7-0.4s-1-0.7-1.3-1.2
				c-0.3-0.6-0.5-1.2-0.5-1.9c0-1.1,0.4-2,1.2-2.7c0.7-0.6,1.6-0.9,2.8-0.9c0.3,0,0.5,0,0.8,0.1
				C688.4,317.1,688.8,317.2,689.2,317.3z"/>
			
				<linearGradient id="SVGID_27_" gradientUnits="userSpaceOnUse" x1="689.658" y1="571.45" x2="694.6897" y2="571.45" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st30" d="M694.7,322.6l-0.1,0.4l-0.1,0.5v0.4h-0.4h-2.8h-1.1c0-0.1,0-0.3,0.1-0.4c0-0.3,0.1-0.8,0.1-1.4V319
				c0-0.4,0-0.8-0.1-1c0-0.2-0.2-0.3-0.5-0.3h-0.1l0.2-0.5h1.1h2.9c0,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0l-0.1,0.4l-0.1,0.5l-0.1,0.3
				h-0.2c-0.1-0.2-0.3-0.3-0.5-0.4s-0.5-0.1-0.9-0.1c-0.1,0-0.3,0-0.6,0v1v1h1.1c0.2,0,0.6,0,1-0.1v0.2v0.4v0.3
				c-0.5,0-0.9-0.1-1.3-0.1c-0.3,0-0.5,0-0.8,0v1.2c0,0.4,0,0.7,0.1,0.8c0,0.1,0.1,0.2,0.1,0.2c0.1,0.1,0.5,0.2,0.9,0.2
				c0.5,0,0.8-0.1,1.1-0.2c0.1-0.1,0.3-0.2,0.5-0.3L694.7,322.6z"/>
			
				<linearGradient id="SVGID_28_" gradientUnits="userSpaceOnUse" x1="695.0804" y1="571.45" x2="701.7131" y2="571.45" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st31" d="M701.7,323.9h-0.6h-0.6c-0.3-0.5-0.5-0.8-0.7-1l-3.1-4v0.5v2.6c0,0.5,0,1,0.1,1.4c0,0.1,0.1,0.3,0.1,0.4
				h-0.4H696h-0.4c0-0.1,0-0.3,0.1-0.4c0-0.3,0.1-0.8,0.1-1.4v-3c0-0.5,0-0.8-0.1-1c0-0.2-0.2-0.3-0.5-0.3h-0.1l0.2-0.5h0.4h1.1h0.6
				c0.3,0.5,0.5,0.8,0.7,1.1l2.5,3.4v-2.5c0-0.6-0.1-1.1-0.1-1.5c0-0.2-0.1-0.3-0.1-0.4h0.4h0.5h0.4c0,0.1-0.1,0.3-0.1,0.4
				c0,0.3-0.1,0.8-0.1,1.6v2.9c0,0.1,0,0.2,0,0.6C701.6,323,701.6,323.5,701.7,323.9z"/>
			
				<linearGradient id="SVGID_29_" gradientUnits="userSpaceOnUse" x1="702.1127" y1="571.55" x2="708.345" y2="571.55" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st32" d="M708.3,317.2l-0.1,0.4l-0.1,0.5v0.3h-0.2c-0.1-0.2-0.3-0.3-0.5-0.4c-0.3-0.1-0.6-0.1-1-0.1
				c-0.2,0-0.3,0-0.4,0v0.9v3.2c0,0.8,0.1,1.4,0.2,1.8h-0.4h-1h-0.4c0-0.1,0-0.3,0.1-0.4c0-0.3,0.1-0.8,0.1-1.4v-3.1
				c0-0.3,0-0.5,0-0.6s0-0.2,0-0.5c-0.5,0-0.9,0-1.2,0.1c-0.1,0-0.2,0.1-0.4,0.1c-0.2,0.1-0.4,0.2-0.6,0.4l-0.2-0.1V318l0.1-0.5
				v-0.4h0.4c1.2,0,2,0,2.5,0s1.4,0,2.6,0L708.3,317.2z"/>
			
				<linearGradient id="SVGID_30_" gradientUnits="userSpaceOnUse" x1="708.3158" y1="571.6" x2="714.8528" y2="571.6" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st33" d="M714.9,323.7L714.9,323.7l-0.4,0.1h-1.6l-0.1-0.2l-0.1-0.3c0-0.1-0.1-0.2-0.2-0.3c-0.4-0.7-0.7-1.3-1-1.8
				c-0.1-0.2-0.2-0.4-0.3-0.4c-0.1-0.1-0.2-0.1-0.3-0.1h-0.1l0.1-0.3v-0.3c0.1,0,0.2,0,0.3,0c0.3,0,0.6-0.1,0.8-0.3
				c0.2-0.2,0.3-0.5,0.3-0.9c0-0.8-0.4-1.2-1.2-1.2c-0.1,0-0.3,0-0.6,0c0,0.5,0,0.8,0,1v3.2c0,0.8,0.1,1.4,0.2,1.8h-0.4h-1h-0.4
				c0-0.1,0-0.3,0.1-0.4c0-0.3,0.1-0.8,0.1-1.4v-3.1c0-0.4,0-0.8-0.1-1c0-0.2-0.2-0.3-0.5-0.3h-0.1l0.2-0.5h0.6h2.8
				c0.6,0,1.1,0.1,1.4,0.4c0.3,0.3,0.4,0.6,0.4,1.1c0,0.8-0.4,1.3-1.3,1.6l0.2,0.4c0.3,0.5,0.8,1.2,1.3,2.1c0.1,0.2,0.3,0.4,0.4,0.6
				C714.6,323.5,714.7,323.6,714.9,323.7z"/>
			
				<linearGradient id="SVGID_31_" gradientUnits="userSpaceOnUse" x1="714.4337" y1="571.7" x2="721.2473" y2="571.7" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st34" d="M721.2,323.8h-0.4h-1h-0.6c0-0.4-0.1-0.9-0.3-1.4l-0.3-0.8H718h-1h-0.7l-0.3,0.8c-0.1,0.4-0.2,0.7-0.2,0.9
				c0,0.1,0,0.3,0.1,0.5h-0.3h-0.8h-0.4v-0.1c0.2-0.3,0.3-0.6,0.5-1c0.1-0.2,0.1-0.4,0.3-0.6l1.5-3.9c0.2-0.4,0.3-0.8,0.4-1.2h0.4
				l0.8-0.1l0.4-0.1c0.1,0.3,0.1,0.6,0.3,1l1.5,4.2c0.1,0.3,0.3,0.8,0.5,1.3C721.1,323.6,721.2,323.7,721.2,323.8z M718.5,320.9
				l-0.9-2.8l-1,2.8h0.6h0.7H718.5z"/>
			
				<linearGradient id="SVGID_32_" gradientUnits="userSpaceOnUse" x1="721.3332" y1="571.45" x2="726.3171" y2="571.45" gradientTransform="matrix(1 0 0 -1 0 892)">
				<stop  offset="0" style="stop-color:#0E2D2A"/>
				<stop  offset="1" style="stop-color:#20423D"/>
			</linearGradient>
			<path class="st35" d="M726.3,322.6l-0.1,0.4l-0.1,0.5l-0.1,0.4h-0.4h-2.8h-1.1c0-0.1,0-0.3,0.1-0.4c0-0.3,0.1-0.8,0.1-1.4V319
				c0-0.4,0-0.8-0.1-1c0-0.2-0.2-0.3-0.5-0.3h-0.1l0.2-0.5h0.4h1.1h0.5c-0.1,0.6-0.1,1.2-0.1,1.6v3c0,0.1,0,0.3,0,0.4
				c0,0.3,0.1,0.5,0.2,0.6c0.1,0.1,0.4,0.2,0.9,0.2s0.8-0.1,1.1-0.2c0.1-0.1,0.3-0.2,0.5-0.4L726.3,322.6z"/>
		</g>
	</g>
</g>
</svg>

                    <div class="pointer-stroke"></div>
                        
                    </div>
                    </a>
                     <svg class="map-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 1200 889.11"   xml:space="preserve">

                        <g>
                            <path class="st-line" d="M362.6,442.1l45.9-29.8c0.3-0.2,6-3.7,12.3-4.2c5.5-0.5,36.8-5.8,38.3-6l140-30.1l30.6-7.6
                                    c0.1,0,13.7-2.8,22.4-9.1c9.3-6.8,57.2-35.6,59.2-36.8l0.4-0.2c0.2-0.1,5.4-2.6,4.4-13.2c-0.2-2.5-0.5-5.6-0.8-9.1
                                    c-1.6-16.8-4-42.3-0.4-61.8"/>
                                <path class="st-line" d="M328.6,411.5l93.2-111.1c1.8-2.1,44.4-52.8,96.5-65.3c30.8-7.8,80.6-23.9,81.1-24c0.4-0.1,0.9-0.2,1.3-0.1
                                    l127.5,24.6l32,5.8l37,0.9c0.4,0,0.8,0.1,1.2,0.3l61.3,32.8c0.3,0.2,0.6,0.4,0.8,0.7c0.2,0.2,16.2,20.3,24.6,32.8
                                    c8.2,12.1,21,29.5,21.6,30.4c0.6,0.8,6,8.1,5.6,19.8c0,0.8,0.1,1.7,0.1,2.5c0.2,6.4,0.4,13.6,8.2,27c5.4,8,45.9,65,49,69.4
                                    c1.2,1,7.3,6.2,12.9,8.2c1.2,0.4,2.8,0.9,4.7,1.4c8.2,2.3,22,6.3,32.2,17.3c7.9,9.1,16.5,17.2,34.9,17.9c0,0,0.1,0,0.1,0
                                    c17.9,1.4,39.2,3.6,41.1,3.8c2.2,0,23.8,0.7,33.8,16.4c10.2,14.7,46.9,194.5,70.7,275.2"/>
                                <path class="st-line" d="M0,555.9l162.1-233.7c0,0,9.8-52.9,49.9-65.1c13.6-3.9,19.3-5.6,46.5-4.6s134.4,0.8,134.4,0.8l68.5-1.3
                                    c0,0,41.9-6.6,66.9-35.2c8.7-8.1,56.4-62.3,102.2-72.9c42.7-9.5,315.9-76.2,315.9-76.2l61.3-14.4c0,0,15.5-4.5,29.9-3
                                    c12.1,0.4,30.7-2.7,46.2-11.8c15.1-12.9,38.1-37.9,38.1-37.9"/>
                        </g>
                        </svg>
                   <!--  <img class="pointer" src="<?php echo get_stylesheet_directory_uri(); ?>/images/map/pointer-01.svg" alt="Vị trí dự án">-->
                </div>
            </div>
        </div>

    </div>
</div>
<?php endwhile; ?>
<?php endif; ?>

<?php //Sec tien ich
        if ( ! have_rows( 'tienich' ) ) {
        return false;
        }

        if ( have_rows( 'tienich' ) ) : ?>

<?php while ( have_rows( 'tienich' ) ) : the_row(); 
        $bg = get_sub_field('background');
        $head = get_sub_field( 'heading' );
        $imgtext = get_sub_field( 'Image_text_title');
        $slider = get_sub_field( 'slider' );
        $slider_mobi = get_sub_field('slider_mobi');

        ?>
<?php // sec tien ich du an?>
<div class="home-section sectu tienich " >
     <div class="bongtop"></div>
 <!--   <?php if( !empty( $imgtext ) ): ?>
    <div class="display-4 text-lux text-center line-wave mb-4 headtienich ">
        <img class="imgtext text-center col-md-4 mb-3" src="<?php echo esc_url($imgtext['url']); ?>" alt="<?php echo $head; ?>">
    </div>
    <?php else:  ?>
    <h2 class="display-4 text-lux text-center line-wave mb-4 headtienich"> <?php echo $head; ?></h2>
    <?php endif; ?>-->

    <?php 
        if (!wp_is_mobile() || empty($slider_mobi)) {
     
           echo do_shortcode($slider);
        } else {
        
            echo  do_shortcode($slider_mobi);
        }
    ?>
<!--<div class="bongbot"></div>-->
</div>
<?php endwhile; ?>
<?php endif; ?>



<?php // sec tien ich du an?>
<div class="home-section secnam matbang " >
    <div class="bongtop"></div>
    <div id="mbfull" >
        <div id="mbfullbg"></div>
    <svg version="1.1" id="svgmb" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 2000 1125" style="enable-background:new 0 0 2000 1125;" xml:space="preserve">

<path id="mbblue" class="st0 mapmb" d="M881.6,617.2c0,0-32,11.1-36.5,12.5c-18.6,5.4-27.8,3.1-45.4-8.2c-17.6-11.3-101.6-70.4-101.6-70.4l2.1-13.4
	c0,0-4.9-0.7-3.3-3.1c1.6-2.4,4.2-6.4,4.2-6.4l1.2-32.2l1.6-3.8h4.5l5.6-6.6l35.5-6.6l14.6,8l4-6.6l2.8-0.5l4.9-6.6l46.4-5.9
	l120.7,60v4.9l4,0.9l0.9,42.6l-29.2-3.3l-23.8,7.5l-1.9,6.8l-9.9,1.6L881.6,617.2z"/>
<path id="mbred" class="st1 mapmb" d="M1187.4,724.8l13.1-122.5v-41.6l-185.6-14.8l-7.1,1.8l-4.2,4.9l-14.5,0.7l-26.8,6.4l-2.8,2.9l-6.4,14.4
	l-32.8-2.1l-23.6,7.1l-2.1,7.4l-8.5,1.8l-2.8,57.2c0,0-7.4,9.5,3.2,13.4c10.6,3.9,91.8,27.2,105.9,29.6
	C1006.3,693.7,1187.4,724.8,1187.4,724.8z"/>
<polygon id="mbgreen" class="st2 mapmb" points="1172.5,496.4 1172.5,516.9 1175,539.1 1170.1,556.1 1014.1,542.3 1007.4,545.8 1001.4,550 992.9,548.3 
	960.8,556.1 950.5,573 952.6,505.9 959.4,498.2 1109.7,508.1 1120.3,484.1 1158.8,486.5 "/>
</svg>
       <div id="points" class="row ">
           
              <a href="" class="point-h blue-diamond act2" data-toggle="modal" data-target="#blue-diamond">Phố <strong>Blue Diamond</strong></a>
              <a href="" class="point-h red-diamond act3" data-toggle="modal" data-target="#red-diamond">Phố <strong>Red Diamond</strong></a>
              <a href="" class="point-h green-diamond act4" data-toggle="modal" data-target="#green-diamond ">Phố <strong>Green Diamond</strong></a>
              <a href="" class="point-h congvien act5" data-toggle="modal" data-target="#congvien">Công viên</a>            
              <a href="" class="point-h congvien khu2 act5" data-toggle="modal" data-target="#congvien">Công viên</a>            
                 <?php // echo do_shortcode('[matbang]'); ?>
        </div>
    </div>

<!--<img id="mbfullbg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/mb.svg">-->

    <?php 
if (!wp_is_mobile()) { 
//echo do_shortcode('[matbang]'); ?>
       <div class="container-fluid ">
             <div class="row ">
            <!--<h2 class="display-4 text-lux text-center line-wave mb-4 headtienich"> MẶT BẰNG</h2>-->
           </div>
          
        </div>
<?php } else { ?>
    <div class="mobile-canho  bot-dot bot_dot_mobile">

        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-1">
                <span class="circle"></span> 01 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>65.02 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>59.05 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct"> [Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot ">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-2">
                <span class="circle"></span> 02 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>67.27 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>61.34 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot ">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-3">
                <span class="circle"></span> 03 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>90.55 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>82.82 m<sup>2</sup></span></p>
                <p>3 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-4">
                <span class="circle"></span> 04 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>92.68 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>84.96 m<sup>2</sup></span></p>
                <p>3 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-5">
                <span class="circle"></span> 05 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>79.93 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>72.65 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-6">
                <span class="circle"></span> 06 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>68.09 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>61.74 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-7">
                <span class="circle"></span> 07 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>67.57 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>61.93 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-8">
                <span class="circle"></span> 08 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>67.57 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>61.89 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-9">
                <span class="circle"></span> 09 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>67.57 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>61.99 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-10">
                <span class="circle"></span> 10 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>61.98 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>55.15 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-11">
                <span class="circle"></span> 11 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>62.02 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>56.28 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-12">
                <span class="circle"></span> 12 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>62.78 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>57.11 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-13">
                <span class="circle"></span> 13 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>71.14 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>64.46 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-14">
                <span class="circle"></span> 14 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>71.14 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>64.43 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-15">
                <span class="circle"></span> 15 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>62.61 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>57.52 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-16">
                <span class="circle"></span> 16 </div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>51.30 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>46.98 m<sup>2</sup></span></p>
                <p>1 PN - 1 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-17">
                <span class="circle"></span> 17</div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>67.46 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>61.34 m<sup>2</sup></span></p>
                <p>2 PN - 2 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
        <div class="box_dot">
            <a class="clickable" href="../can-ho/can-ho-3pn/"><span></span></a>
            <div class="zoom-box pos-18">
                <span class="circle"></span> 18</div>
            <div class="ct_bot_moblie">
                <p class="ares">TT: <span>60.56 m<sup>2</sup></span></p>
                <p class="ares">LL: <span>54.95 m<sup>2</sup></span></p>
                <p>2 PN - 1 WC</p>
                <p class="detail_ct">[Xem chi tiết]</p>
            </div>

        </div>
    </div>

    <?php  }
?>
    <div class="bongbot"></div>
</div>



<?php 

    // WP_Query arguments
    $args = array(
        'posts_per_page'         => '3',
    );

    // The Query
    $homenews = new WP_Query( $args );
?>
<div class="home-section secbay news " style="background:url(<?php echo get_stylesheet_directory_uri(); ?>/images/1x/bg5.png)  center center/cover no-repeat;">
 <div class="bongtop"></div>
    <div class="container  text-center">



        <div class="display-4 text-lux text-center line-wave mb-4 col-md-4">
            <img class="imgtext text-center headtintuc" src="<?php echo get_stylesheet_directory_uri(); ?>/images/SVG/h-tintuc.svg" alt="<?php echo $head; ?>">
        </div>

        <div class="row d-flex mt-2 boxnews">

            <?php    // The Loop
                if ( $homenews->have_posts() ) {
                   $i = $j = 1;
                    while ( $homenews->have_posts() ) {
                        $homenews->the_post(); 
                        
                        $j = $i + 1;
             
            ?>



            <div class="col-md-4 col-sm-12 p-4 " style="order: <?php  echo $i; ?>">
                <a class="news-click" href="<?php echo the_permalink();?>"> </a>
                <?php the_title( '<h2 class="entry-title content text-1 px-2 mb-0 pt-2 ">', '</h2>' ); ?>
                <p class=" text text-justify   mb-0 text-white p-2"><?php echo substr(strip_tags($post->post_content), 0, 200);?> </p>
            </div>
            <div class="col-md-4 col-sm-12 m-0 p-0 hinh" style="height: 210px;order: <?php  echo $j; ?>;background-image:url(<?php echo get_the_post_thumbnail_url( $post->ID, 'large'); ?>);">

            </div>

            <?php   $i = $j + 1;   
                      
                    } //end while  ?>
        </div>
        <?php     } 
                
            

            // Restore original Post Data
            wp_reset_postdata();
        ?>


    </div>

    <!--<div class="wave-bottom ani2 animated"></div>-->

</div>
<?php if( have_rows('Partners') ):  $i = 1; ?>
<div class="home-section sectam partners">
 <div class="bongtop"></div>
    <div class="container">
        <div id="row1" class="row mb-2">

            <?php while( have_rows('Partners') ): the_row(); 

		// vars
		$image = get_sub_field('logo_partner');
		$title = get_sub_field('title_partner');
		$link = get_sub_field('link_partner');
        $numrows = count( get_sub_field( 'logo_partner' ) );
         if( $i==1 || $i == 2) { 
		?>
            <div class="col p-5 text-center ani1 ">
                <h3 class="text-normal text-uppercase text-white mb-4"><?php echo $title; ?></h3>
                <a href="<?php echo $link; ?>"><img class="cdt" src="<?php echo $image['url']; ?>" /></a>
            </div>


            <?php 
          if( $i == 2) { echo '</div><div id="row2" class="row ">'; }
         } else { ?>

            <div class="col m-0 pt-5 text-center ani<?php echo $i; ?> ">
                <h5 class="text-normal text-uppercase text-white mb-3"><?php echo $title; ?></h5>
                <a href="<?php echo $link; ?>"><img class="partner" src="<?php echo $image['url']; ?>" /></a>
            </div>



            <?php } ?>
            <?php $i++; endwhile; ?>
        </div>
    </div>

</div>
<?php endif; ?>

<?php if( have_rows('lienhe') ):  ?>
<div class="home-section secchin lienhe">
 <div class="bongtop"></div>
    <div class="container">
        <div class="row">

            <div class="col col py-3 px-lg-5 ">
                <h3 class=" text-lux text-left mb-4 mt-2"> ĐĂNG KÝ NHẬN THÔNG TIN</h3>
                <?php echo do_shortcode('[contact-form-7 id="134"]');?>
            </div>
            <div class="col text-lux col py-3 px-lg-5">
                <?php while( have_rows('lienhe') ): the_row(); 
		// vars
		$tenvp = get_sub_field('ten_vp');
		$diachi = get_sub_field('diachi');
		$email = get_sub_field('email_vp');
		$dt = get_sub_field('dt_vp');
        if ($tenvp):
		?>
                <div class="box-info">
                    <h3 class="heading text-uppercase  mt-2 "><?php echo $tenvp; ?></h3>
                    <p class="p-0 m-0"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $diachi; ?></p>
                    <?php if ($dt):  ?><p><i class="fa fa-envelope"></i> <?php echo $dt; ?></p> <?php endif; ?>
                    <?php if ($email):  ?><p><i class="fa fa-phone-square"></i> <?php echo $email; ?></p><?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <!--  <div class="wave-bottom dark ani3 animated"></div>-->
    </div>
</div>
<?php endif; ?>
<?php

get_footer();
