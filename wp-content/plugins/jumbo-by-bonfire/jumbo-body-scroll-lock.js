/* when menu button or custom activator used */
jQuery('.jumbo-menu-button, .jumbo-custom-activator, .jumbo-by-bonfire ul li a').on('click', function(e) {
e.preventDefault();
	if(jQuery('.jumbo-by-bonfire-wrapper').hasClass('jumbo-menu-active'))
	{

		/* disable browser scroll */
		var current = jQuery(window).scrollTop();
        jQuery(window).scroll(function() {
            jQuery(window).scrollTop(current);
        });

		return false;

	} else {

		/* enable browser scroll */
		jQuery(window).off('scroll');

		return false;
	}
});

/* enable when ESC button pressed */
jQuery(document).keyup(function(e) {
	if (e.keyCode == 27) { 

		/* enable browser scroll */
		jQuery(window).off('scroll');

		return false;

	}
});